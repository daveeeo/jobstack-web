module.exports = {
  printWidth: 80,
  semi: false,
  singleQuote: true,
  trailingComma: 'all',
  jsxBracketSameLine: false,
  tabWidth: 2,
  bracketSpacing: true,
  endOfLine: 'lf',
  arrowParens: 'always',
}
