// import * as React from 'react'
// import {Route} from 'react-router';
// import firebaseService from '../services/firebaseService'

// export interface Props {
//   children: React.ReactNode,
//   jobId: string,
//   // history: History
// }

// export interface State {
//   job: object,
//   loading: boolean,
//   docId: string
// }

// export default class JobContainer extends React.Component<Props & Route, State> {
//   state = {
//     job: null,
//     loading: true,
//     docId: null
//   }

//   componentDidMount() {
//     const { jobId } = this.props
//     jobId && this.fetchJob(jobId)
//   }

//   componentDidUpdate(prevProps) {
//     const { jobId } = this.props

//     if (jobId !== prevProps.jobId) {
//       jobId && this.fetchJob(jobId)
//     }
//   }

//   fetchJob = jobId => {
//     const { history } = this.props
//     const { store } = firebaseService

//     this.setState({ job: null, loading: true })

//     store()
//       .collection('jobs')
//       .doc(jobId)
//       .get()
//       .then(doc => {
//         console.log('doc: ', doc)
//         if (doc.exists) {
//           const docId = doc.id
//           const job = doc.data()

//           this.setState({
//             loading: false,
//             docId,
//             job,
//           })
//         } else {
//           history.push('/404')
//         }
//       })
//       .catch(err => console.log(err))
//   }

//   render() {
//     const { children } = this.props
//     return children(this.state)
//   }
// }
