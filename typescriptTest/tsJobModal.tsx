import * as React from 'react'
import styled from 'styled-components/macro'
import { IoIosClose as CloseButton } from 'react-icons/io'
import { Transition } from 'react-transition-group'
import JobContainer from '../containers/JobContainer'
import JobDescription from './JobDescription'
import './jobpanel.css'
const onClickOutside = require('react-onclickoutside').default

const duration = 300

const PanelContainer = styled.aside`
  position: fixed;
  width: 38.5%;
  height: calc(100% - var(--header-height));
  top: calc(var(--header-height) + 2px);
  right: 0;
  background: white;
  z-index: 1;
  will-change: transform;
  /* transition: transform 300ms cubic-bezier(0.25, 0.46, 0.45, 0.94); */
  transition: transform 300ms cubic-bezier(0.19, 1, 0.22, 1);
  transform: translate3d(100%, 0, 0);
  box-shadow: rgba(0, 0, 0, 0.05) -5px 17px 25px;

  &:focus {
    outline: none;
  }
`

interface JobPanelProps {
  open: boolean,
  jobId: string,
  enableOnClickOutside: () => void,
  disableOnClickOutside: () => void,
  close: () => void
}

class JobPanel extends React.Component<JobPanelProps, {}> {
  handleClickOutside = () => {
    const { close } = this.props
    console.log('closing the menu')
    close()
  }

  handleEscKey = (e: KeyboardEvent) => {
    const { close } = this.props
    if (e.keyCode === 27) {
      close()
      console.log('esc key pressed')
    }
  }

  render() {
    const {
      open,
      jobId,
      enableOnClickOutside,
      disableOnClickOutside,
    } = this.props

    return (
      <Transition
        in={open}
        appear
        timeout={duration}
        onEnter={() => {
          enableOnClickOutside()
          window.addEventListener('keydown', this.handleEscKey, false)
        }}
        onExited={() => {
          disableOnClickOutside()
          window.removeEventListener('keydown', this.handleEscKey, false)
        }}
      >
        {state => (
          <PanelContainer className={`slide slide-${state}`}>
            <JobContainer jobId={jobId}>
              {props => <JobDescription isModal {...props} />}
            </JobContainer>
            <CloseButton data-testid="modal-close" />
          </PanelContainer>
        )}
      </Transition>
    )
  }
}

export default onClickOutside(JobPanel)
