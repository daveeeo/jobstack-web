const config = {
  algoliaAppId: process.env.REACT_APP_ALGOLIA_APP_ID,
  algoliaApiKey: process.env.REACT_APP_ALGOLIA_SEARCH_API_KEY,
  algoliaIndexName: process.env.REACT_APP_ALGOLIA_INDEX_NAME,
  firebaseProjectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  firebaseApiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  firebaseAuthDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  firebaseStorageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  firebaseMessagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  googleAnalyticsTrackingId: process.env.REACT_APP_GOOGLE_ANALYTICS_TRACKING_ID,
}

export default config