const fetch = require('node-fetch')
const algoliaSitemap = require('algolia-sitemap');

const algoliaConfig = {
  appId: process.env.REACT_APP_ALGOLIA_APP_ID,
  apiKey: process.env.REACT_APP_ALGOLIA_SEARCH_API_KEY,
  indexName: process.env.REACT_APP_ALGOLIA_INDEX_NAME,
}

/**
 *  Function to transform a hit into its sitemap link
 */

const hitToParams = (hit) => {
  const url = ({ slug, objectID }) => `https://jobstack.co/job/${slug}/${objectID}`
  const loc = url({ slug: hit.slug, objectID: hit.objectID })
  const lastmod = new Date().toISOString()
  const priority = Math.random()

  return {
    loc,
    lastmod,
    priority
  }
}

algoliaSitemap({
  algoliaConfig,
  sitemapLoc: 'https://jobstack.co',
  outputFolder: 'build',
  hitToParams
})
  .then(() => console.log('Sitemap successfully generated'))
  .then(() => Promise.all([
    fetch('http://www.google.com/webmasters/sitemaps/ping?sitemap=https://jobstack.co/sitemap-index.xml'),
    fetch('http://www.bing.com/webmaster/ping.aspx?siteMap=https://jobstack.co/sitemap-index.xml')
  ]))
  .then(() => {
    console.log('Successfully pinged search engines')
  })
  .catch((err) => console.log(err))

