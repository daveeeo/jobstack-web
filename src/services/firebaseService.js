import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
import config from '../utils/config'

// Initialize Firebase
const firebaseConfig = {
  apiKey: config.firebaseApiKey,
  authDomain: config.firebaseAuthDomain,
  projectId: config.firebaseProjectId,
  storageBucket: config.firebaseStorageBucket,
  messagingSenderId: config.firebaseMessagingSenderId,
}

const settings = { timestampsInSnapshots: true }

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig)
    this.firebase = firebase
    this.store = firebase.firestore
    this.auth = firebase.auth
    this.store().settings(settings)
  }

  get jobs() {
    return firebase.firestore().collection('jobs')
  }

  get users() {
    return firebase.firestore().collection('users')
  }
}

export default new Firebase()
