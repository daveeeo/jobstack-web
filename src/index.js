import React from 'react'
import ReactDOM from 'react-dom'
import { ToastProvider, withToastManager } from 'react-toast-notifications';
import App from './containers/App'
import * as serviceWorker from './serviceWorker'
import { AuthProvider } from './containers/AuthContext'
import { ToastContainer, Toast } from './components/ui/Elements/ToastNotification';

import './styles/flexboxgrid.css'
import './styles/algolia-reset.css'
import './styles/index.css'

const AuthProviderWithToasts = withToastManager(AuthProvider)

ReactDOM.render(
  <ToastProvider placement='bottom-right' autoDismissTimeout={3000} components={{ ToastContainer, Toast }}>
    <AuthProviderWithToasts>
      <App />
    </AuthProviderWithToasts>
  </ToastProvider>,
  document.getElementById('root'),
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register()
