import React from 'react'
import { Switch, Route, Redirect } from "react-router-dom";
import JobContainer from './JobContainer';
import Home from './Home'
import { AuthConsumer } from './AuthContext'
import Auth from './Auth';
import Account from './Account';
import SavedJobs from './SavedJobs';
import Contact from './Contact'

// Custom route that provides Component with AuthContext
const RouteWithAuthContext = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthConsumer>
        {context => <Component {...props} {...context} />}
      </AuthConsumer>
    )}
  />
)

// Custom route that protects Components that require authentication
const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthConsumer>
        {context =>
          context.authStatusReported && context.isAuth ? (
            <Component {...props} {...context} />
          ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location },
              }}
            />
          )
        }
      </AuthConsumer>
    )}
  />
)

class ModalSwitch extends React.Component {

  previousLocation = this.props.location

  componentDidUpdate(prevProps) {
    const { history, location } = this.props

    // When user navigates forward, Check if location.state.modal exists, and if it does, set this.previousLocation to prevProps.location so the modal can be displayed over the current page 
    if (history.action !== "POP" && ( location.state && location.state.modal )) {
      this.previousLocation = prevProps.location
    } else {
      this.previousLocation = location
    }
  }

  render() {
    const { location } = this.props

    // If location.state.modal === true, this will tell the Switch to stay on the current page, allowing for us to display the modal on-top of it.
    const isModal = !!(location.state && location.state.modal && this.previousLocation !== location)

    return (
      <div>
        <Switch location={isModal ? this.previousLocation : location}>
          <RouteWithAuthContext path="/" exact component={Home} />
          <RouteWithAuthContext path="/job/:jobSlug/:objectID" exact component={JobContainer} />
          <RouteWithAuthContext
            path="/contact"
            exact
            component={Contact}
          />
          <RouteWithAuthContext
            path="/login"
            exact
            component={Auth}
          />
          <RouteWithAuthContext
            path="/signup"
            exact
            component={Auth}
          />
          <RouteWithAuthContext
            path="/reset-password"
            exact
            component={Auth}
          />
          <ProtectedRoute
            path="/account"
            exact
            component={Account}
          />
          <ProtectedRoute path="/saved" component={SavedJobs} />
        </Switch>
      </div>
    )
  }
}

export default ModalSwitch