/* eslint-disable no-unused-expressions */
import React, { Component } from 'react'
import { matchPath } from 'react-router'
import { Route, Redirect, Link } from 'react-router-dom'
import Helmet from 'react-helmet-async'
import { MdInbox as EmptyStackIcon } from "react-icons/md";
import MediaQuery from 'react-responsive';
import styled from 'styled-components/macro'
import media from '../styles/style-media'
import Subnav from '../components/Subnav'
import Panel from '../components/Panel'
import { AuthContext } from './AuthContext'
import 'moment-timezone'
import JobContainer from './JobContainer';

const IconWrapper = styled.span`
  display: inline-flex;
  width: 120px;
  height: 120px;
  border-radius: 50%;
  justify-content: center;
  align-items: center;
  background-color: white;
  margin-bottom: 3rem;
  padding: 1.5rem;
  box-shadow: var(--box-shadow-container-light);

  svg {
    fill: var(--color-primary);
    width: 100%;
    height: 100%;
  }
`

const Wrapper = styled.div`
  position: relative;
  width: 100%;

  ${media.desktop`
    display: grid;
    grid-template-columns: 300px 1fr;
  `};

  ${media.giant`
    display: grid;
    grid-template-columns: 340px 1fr;
  `};
  
  .job-header {
    .btn-back {
      ${media.desktop`
        display: none;
      `};
    }

    .job-meta {
      display: none;

      ${media.tablet`
        display: inherit;
      `};
    }
  }
`

export default class SavedJobs extends Component {
  static contextType = AuthContext

  componentDidUpdate(prevProps) {
    const { location, match, savedJobs } = this.props
    const { saveActiveJob } = this.context

    // Save activeJobIndex to Context so if the user navigates away from SavedJobs and returns, the previously selected job is rendered by default
    if (prevProps.location !== location) {
      const activeJob = matchPath(location.pathname, {
        path: `${match.url}/:jobSlug/:objectID`
      })

      const activeJobIndex = savedJobs.findIndex(el => activeJob && el.objectID === activeJob.params.objectID)

      saveActiveJob(activeJobIndex)
    }
  }

  render() {
    const { activeJobIndex } = this.context
    const { location, match, savedJobs } = this.props

    const savedJobsByDate = savedJobs.sort((a, b) => b.timestamp - a.timestamp)

    if (!savedJobs.length) {
      return (
        <div className='row center-xs middle-xs text-center'>
          <div className='col-xs-12' style={{ padding: '0 1.5rem' }}>
            <Helmet>
              <title>Saved Jobs - jobstack.co</title>
              <meta name="description" content="JobStack finds the latest programming jobs from around the world to provide you with a fast and simple job searching experience." />
            </Helmet>
            <IconWrapper>
              <EmptyStackIcon />
            </IconWrapper>
            <h1 className='title'>Your job stack is empty</h1>
            <p className='subtitle'>Start saving jobs to your stack so you can keep track of the jobs you're interested in.</p>
            <Link to='/' className='btn btn--primary margin-top'>Browse jobs</Link>
          </div>
        </div>
      )
    }

    return (
      <Wrapper>
        <MediaQuery minWidth={1024}>
          {(matches) => {
            if (matches) {
              return (
                <>
                  <Subnav
                    list={savedJobsByDate.map(
                      ({ objectID, eventID, timestamp, data }) => ({
                        objectID,
                        eventID,
                        title: data.title,
                        slug: data.slug,
                        datePosted: data.datePosted,
                        companyLogo: data.companyLogo,
                        timestamp
                      }),
                    )}
                    {...this.props}
                  />
        
                  {savedJobs.length && location.pathname === '/saved' ? (
                    <Redirect to={`${match.path}/${savedJobsByDate[activeJobIndex].data.slug}/${savedJobsByDate[activeJobIndex].objectID}`} />
                  ) : null}

                  <Route
                    path={`${match.url}/:jobSlug/:objectID`}
                    render={props => (
                      <Panel type="content">
                        <JobContainer isSavedJobs {...props} />
                      </Panel>
                    )}
                  />
                </>
              )
            }

            return (
              <>
                {location.pathname === '/saved' ? (
                  <Subnav
                    list={savedJobsByDate.map(
                      ({ objectID, eventID, timestamp, data }) => ({
                        objectID,
                        eventID,
                        title: data.title,
                        slug: data.slug,
                        datePosted: data.datePosted,
                        companyLogo: data.companyLogo,
                        timestamp
                      }),
                    )}
                    {...this.props}
                  />
                ) : (
                  <Route 
                    path='/saved/:jobSlug/:objectID'
                    exact
                    render={(props) => <JobContainer isSavedJobs {...props} />} />
                )}
              </>
            )
          }}
        </MediaQuery>
      </Wrapper>
    )
  }
}
