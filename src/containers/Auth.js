/* eslint-disable no-nested-ternary */
import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import styled, { css } from 'styled-components/macro'
import { GoMarkGithub as Github } from "react-icons/go";
import { Google } from '../components/ui/Icons'
import Input from '../components/ui/Form/Input'
import { FormStyled, Field } from '../components/ui/Form/FormStyles'
import Button from '../components/ui/Elements/Button'
import SignIn from '../components/SignIn'
import withAuthContext from './withAuthContext';
import media from '../styles/style-media'
import Box from '../components/ui/Elements/Box';

const AuthContainer = styled.div`
  display: flex;
  padding: 1rem;
  
  ${props => !props.isModal && css`
    /* margin-top: 1.5rem; */
    flex-basis: 100%;
    ${media.tablet`
      margin-top: 3rem;
      padding: 1.5rem;
      flex-basis: auto;
    `};
  `}

  fieldset {
    > p {
      max-width: 400px;
      margin: auto auto 1rem;
      text-align: center;
    }

    .btn-forgot-password {
      position: absolute;
      right: 10px;
      top: 1px;
      font-size: 0.9rem;
      padding: 0;
    }
  }
`

const AuthButtons = styled.div`
  display: grid;
  margin-bottom: 1rem;

  ${media.tablet`
    grid-template-columns: 1fr 1fr;
    grid-gap: 0 1rem;
    margin-bottom: 2rem;
  `}
  
`

const BottomLinks = styled.div`
  font-size: 0.9rem;
  text-align: center;
  margin: 2rem 0 0.5rem;

  button {
    background: none;
    border: none;
    box-shadow: none;
    cursor: pointer;
    color: var(--color-primary);
    padding: 0;

    &:focus {
      outline: none;
    }
  }
`

const BoxStyled = styled(Box)`
  background-color: white;
  box-shadow: ${props => props.isModal && 'var(--box-shadow-container)' };
  border: ${props => props.isModal && 'none' };
`

class Auth extends Component {

  state = {
    email: '',
    password: '',
    confirmPassword: '',
    submitting: false
  }

  componentDidUpdate(prevProps) {
    const { context, isModal, onCloseModal } = this.props
    
    if ((prevProps.context.isAuth !== context.isAuth) || (prevProps.context.isAnonymous !== context.isAnonymous)) {
      if (isModal) {
        onCloseModal()
      }
    }
  }

  handleInput = (e) => {
    const { value, name } = e.target

    this.setState({
      [name]: value,
    })
  }

  validateForm = (formType) => {
    const { email, password } = this.state

    if (formType.includes('reset-password')) {
      return email.length > 0
    } 
    
    if (formType.includes('reset-confirmed')) {
      return password.length > 0
    }

    return (
      email.length > 0 && password.length > 0
    )
  }

  handleSubmit = (e, formType) => {
    e.preventDefault()

    const { email, password } = this.state
    const { context, actionCode, confirmedEmail, isModal, onCloseModal } = this.props

    this.setState({ submitting: true })

    formType.includes('signup') && context.signUp(email, password)
    formType.includes('login') && context.signIn('manual', email, password)
    formType.includes('reset-password') && (
      context.sendPasswordResetEmail(email)
        .then(() => {
          isModal && onCloseModal()
          this.setState({ submitting: false })
        })
    )
    formType.includes('reset-confirmed') && context.handleConfirmPasswordReset(actionCode, confirmedEmail, password)
  }

  handleOAuthClick = (e, provider) => {
    e.stopPropagation()
    const { context } = this.props
    context.signIn(provider)
  }

  render() {
    const { location, authType, isModal, onOpenModal, context } = this.props
    const { email, password, confirmPassword, submitting } = this.state
    const formType = authType || location.pathname
    const isSignup = formType.includes('signup')
    const isLogin = formType.includes('login')
    const isReset = formType.includes('reset')
    const isResetPassword = formType.includes('reset-password')
    const isResetPasswordConfirmed = formType.includes('reset-confirmed')

    let formHeading; let buttonLabel

    if (isSignup) {
      formHeading = 'Create an account'
      buttonLabel = 'Sign up'
    } else if (isLogin) {
      formHeading = 'Login to your account'
      buttonLabel = 'Login'
    } else if (isResetPassword) {
      formHeading = 'Reset your password'
      buttonLabel = 'Request password reset'
    } else if (isResetPasswordConfirmed) {
      formHeading = 'Enter a new password'
      buttonLabel = 'Reset password'
    }

    if (context.isAuth && !context.isAnonymous) {
      return (
        <Redirect
          to={{
            pathname: '/',
            state: { from: location },
          }}
        />
      )
    }

    return (
      <AuthContainer isModal={isModal}>
        <div className="row" style={{ flex: 1, justifyContent: 'center' }}>
          <div className='col-xs'>
            <BoxStyled border isModal={isModal}>
              <FormStyled
                onSubmit={e => this.handleSubmit(e, formType)}
              >
                <fieldset>
                  <h2>{formHeading}</h2>
                  {isResetPassword && (
                    <p className='text-center'>Enter your email below to request a password reset. We first need to send you a verification email before you can change your password.</p>
                  )}
                  {isResetPasswordConfirmed && (
                    <p className='text-center'>Your new password request has been verified. Please enter a new password below.</p>
                  )}
                  {(!isReset) && (
                    <AuthButtons>
                      <SignIn
                        onClick={(e) => this.handleOAuthClick(e, 'google')}
                        icon={<Google />}
                        text="Sign in with Google"
                      />
                      <SignIn
                        onClick={(e) => this.handleOAuthClick(e, 'github')}
                        icon={<Github />}
                        text="Sign in with Github"
                      />
                    </AuthButtons>
                  )}

                  {!isResetPasswordConfirmed && (
                    <Input
                      name="email"
                      label={!isResetPassword && 'Email'}
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={this.handleInput}
                    />
                  )}
                  
                  {!isResetPassword && (
                    <Field>
                      <Input
                        name="password"
                        label={!isResetPasswordConfirmed && 'Password'}
                        type="password"
                        placeholder={isResetPasswordConfirmed ? 'Enter new password' : 'Enter password'}
                        value={password}
                        onChange={this.handleInput}
                      />
                      {isLogin && (
                        <Link 
                          className='btn-empty btn-forgot-password' to={{
                            pathname: '/reset-password',
                            state: { modal: !!isModal }
                          }}
                          onClick={() => isModal && onOpenModal()}
                        >
                          Forgot your password?
                        </Link>
                      )}
                    </Field>
                  )}

                  {isSignup && (
                    <Input
                      name="confirmPassword"
                      label="Confirm Password"
                      type="password"
                      placeholder="Re-enter password"
                      value={confirmPassword}
                      onChange={this.handleInput}
                    />
                  )}
                  <Button className='btn-large btn-fullwidth' loading={submitting} disabled={!this.validateForm(formType)} type="submit">
                    {buttonLabel}
                  </Button>
                  <BottomLinks>
                    {/* Link to Sign up Modal/Page depending if user is in modal or not */}
                    {isSignup && (
                      <>
                        Already have an account?{` `}
                        <Link 
                          className='btn-empty'
                          to={{
                            pathname: '/login',
                            state: { modal: !!isModal }
                          }}
                          onClick={() => isModal && onOpenModal()}
                        >
                          Login
                        </Link>
                      </>
                    )}

                    {/* Link to Login Modal/Page depending if user is in modal or not */}
                    {isLogin && (
                      <>
                        Don't have an account?{` `}
                        <Link 
                          className='btn-empty'
                          to={{
                            pathname: '/signup',
                            state: { modal: !!isModal }
                          }}
                          onClick={() => isModal && onOpenModal()}
                        >
                          Sign up
                        </Link>
                      </>
                    )}

                    {/* Link to Reset Password Modal/Page depending if user is in modal or not */}
                    {isResetPassword && (
                      <>
                        Remember your password?{` `}
                        <Link 
                          className='btn-empty'
                          to={{
                            pathname: '/login',
                            state: { modal: !!isModal }
                          }}
                          onClick={() => isModal && onOpenModal()}
                        >
                          Login
                        </Link>
                      </>
                    )}
                  </BottomLinks>
                </fieldset>
              </FormStyled>
            </BoxStyled>
          </div>
        </div>
      </AuthContainer>
    )
  }
}

export default withAuthContext(Auth)
