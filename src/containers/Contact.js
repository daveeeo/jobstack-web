import React from 'react'
import LayoutContainer from '../components/ui/Layout'
import Box from '../components/ui/Elements/Box';
import Input from '../components/ui/Form/Input'
import Textarea from '../components/ui/Form/Textarea';
import Button from '../components/ui/Elements/Button';
import { FormStyled } from '../components/ui/Form/FormStyles';

const Contact = () => (
  <LayoutContainer padding="large">
    <div className='row' style={{ flex: '1 0 100%' }}>
      <div className="col-xs-12 col-sm-8">
        <Box border style={{ flexFlow: 'column wrap' }}>
          <h1 className='title'>Send us a message</h1>
          <FormStyled action="https://formspree.io/hello@jobstack.co" method="POST">
            <Input
              name="name"
              label='Name'
              type="text"
              placeholder="Enter your name"
              required
            />
            <Input
              name="_replyto"
              label='Email'
              type="email"
              placeholder="Enter your email"
              required
            />
            <Textarea
              name='message'
              label='Your message'
              placeholder='What would you like to chat with us about?'
              required
            />
            <Button className='btn-large btn-fullwidth' type="submit">
                Send
            </Button>
          </FormStyled>
        </Box>
      </div>
    </div>
  </LayoutContainer>
)

export default Contact