import React from 'react'
import { ModalConsumer } from './ModalContext'

export default function withAppContext(Component) {
  return function WrapperComponent(props) {
    return (
      <ModalConsumer>
        {state => <Component {...props} context={state} />}
      </ModalConsumer>
    )
  }
}
