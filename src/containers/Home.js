import React, { Component } from 'react'
import styled from 'styled-components/macro'
import Helmet from 'react-helmet-async'
import { Configure } from 'react-instantsearch-dom'
import queryString from 'query-string'
import withModalContext from "./withModalContext";
import Listings from '../components/Listings'
import { SearchConsumer } from './SearchContext'
import { RefinementList } from '../components/RefinementList'
import Box from '../components/ui/Elements/Box';
import Footer from '../components/Footer';
import Auth from "./Auth";
import media from '../styles/style-media'

const HomeContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-evenly;
  align-items: flex-start;
  padding: 1.5rem 0.5rem;
  flex-basis: 100%;
  max-width: 1280px;
  
  ${media.tablet`
    margin: 0;
    padding: 2.5rem 1.5rem;
  `};
`

class Home extends Component {

  componentDidMount() {
    const { location, handleVerifyPasswordReset, handleVerifyEmail, context } = this.props

    const queryParameters = queryString.parse(location.search)
    const { mode, oobCode, continueUrl, lang } = queryParameters

    // If user triggers email action handler, handle action accordingly
    switch (mode) {
      case 'resetPassword':
        handleVerifyPasswordReset(oobCode, continueUrl, lang)
          .then((res) => context.onOpenModal(Auth, { 
            authType: 'reset-confirmed',
            confirmedEmail: res,
            oobCode
          }))
        break

      case 'verifyEmail':
        handleVerifyEmail(oobCode, continueUrl, lang)
        break
      default:
        break
    }
  }

  render() {
    return (
      <HomeContainer>
        <Helmet>
          <title>jobstack.co - Programming jobs for Front-end, Full-stack, and Back-end Developers</title>
          <meta name="description" content="JobStack finds the latest programming jobs from around the world to provide you with a fast and simple job searching experience." data-react-helmet="true" />
        </Helmet>
        <div className="col-xs-12 col-md-7" style={{ flexGrow: 1 }}>
          <Configure hitsPerPage={20} aroundLatLngViaIP={false} />         
          <Listings />
        </div>
        <div className="col-xs-12 col-md-4">
          <Box border style={{ marginTop: '47px' }}>
            <SearchConsumer>
              {({ dispatch, filters }) => (
              <>
                <RefinementList
                  attribute="categories"
                  dispatch={dispatch}
                  defaultRefinement={filters}
                />
              </>
              )}
            </SearchConsumer>
          </Box>
          <Footer />
        </div>
      </HomeContainer>
    )
  }
}

export default withModalContext(Home)
