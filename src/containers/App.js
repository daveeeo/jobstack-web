import React from 'react'
import {
  Router,
  Route,
  withRouter,
} from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import ReactGA from 'react-ga';
import Helmet, { HelmetProvider } from 'react-helmet-async'
import styled from 'styled-components/macro'
import algoliasearch from 'algoliasearch/lite'
import { InstantSearch } from 'react-instantsearch-dom'
import config from "../utils/config";
import HeaderComponent from '../components/Header'
import { GlobalStyle } from '../styles/style-utils'
import { SearchProvider } from './SearchContext'
import { ModalProvider } from './ModalContext'
import ModalSwitch from './ModalSwitch';
import ContentWrapper from '../components/ContentWrapper';

const history = createHistory()
ReactGA.initialize(config.googleAnalyticsTrackingId);
history.listen((location, action) => {
  ReactGA.pageview(location.pathname + location.search)
})

const searchClient = algoliasearch(
  config.algoliaAppId,
  config.algoliaApiKey
)

const ModalProviderWithRouter = withRouter(ModalProvider)

const Wrapper = styled.div`
  height: 100%;
  margin: 0;
`

const Header = withRouter(HeaderComponent)

class App extends React.Component {
  render() {
    return (
      <HelmetProvider>
        <Router history={history}>
          <InstantSearch indexName={config.algoliaIndexName} searchClient={searchClient}>
            <Helmet>
              <meta name="description" content="JobStack finds the latest programming jobs from around the world to provide you with a fast and simple job searching experience." />
            </Helmet>
            <ModalProviderWithRouter>
              <SearchProvider>
                <GlobalStyle />
                <Wrapper>
                  <Header {...this.props} />
                  <ContentWrapper>
                    <Route component={ModalSwitch} />
                  </ContentWrapper>
                </Wrapper>
              </SearchProvider>
            </ModalProviderWithRouter>
          </InstantSearch>
        </Router>
      </HelmetProvider>
    )
  }
}

export default App
