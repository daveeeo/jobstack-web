import React, { Component } from 'react'
import styled from 'styled-components/macro'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { withToastManager } from 'react-toast-notifications';
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton'
import firebaseService from '../services/firebaseService'
import Input from '../components/ui/Form/Input'
import { FormStyled, Text, Field } from '../components/ui/Form/FormStyles'
import Button from '../components/ui/Elements/Button'
import Box from '../components/ui/Elements/Box';
import DisplayPicture from '../components/DisplayPicture';
import Loading from '../components/Loading';
import LayoutContainer from '../components/ui/Layout';

const DisplayPictureContainer = styled(Field)`
  display: flex;
  align-items: center;

  label {
    cursor: pointer;

    &:hover {
      text-decoration: underline;
    }
  }
`

const UploadDisplayPicture = ({ 
  displayPicture,
  handleUploadStart, 
  handleUploadError,
  handleUploadSuccess,
  handleProgress,
  isUploading,
  setFieldValue,
  setFieldTouched
}) => (
  <DisplayPictureContainer>
    <DisplayPicture as='div' displayPicture={displayPicture || false} isUploading={isUploading} />
    <div>
      <CustomUploadButton
        accept="image/*"
        name="displayPicture"
        value=''
        randomizeFilename
        storageRef={firebaseService.firebase
          .storage()
          .ref('images/displayPictures')}
        onUploadStart={handleUploadStart}
        onUploadError={handleUploadError}
        onUploadSuccess={(filename) => handleUploadSuccess(filename, setFieldValue, setFieldTouched)}
        onProgress={handleProgress}
        style={{
          display: 'inline-flex',
          color: 'var(--color-primary)',
          padding: 10
        }}
      >
        Upload a new display picture
      </CustomUploadButton>
    </div>
  </DisplayPictureContainer>
)

class Account extends Component {
  state = {
    displayPictureFilename: '',
    isUploading: false,
    progress: 0,
    displayPictureUrl: '',
  }

  onSubmit = (values, { setSubmitting }) => {
    this.updateUserProfile(values, setSubmitting)
  }

  updateUserProfile = (values, setSubmitting) => {
    const { uid, toastManager, dispatch } = this.props
    const { store } = firebaseService
    const user = store()
      .collection('users')
      .doc(uid)

    const { email, displayName, displayPicture, photoUrl } = values

    user
      .update({
        email,
        displayName,
        photoUrl: displayPicture || photoUrl,
      })
      .then(() => {
        setSubmitting(false)

        // Update account details in Context
        dispatch({
          type: 'UPDATE_ACCOUNT_DETAILS',
          newAccountDetails: {
            email,
            displayName,
            photoUrl: displayPicture || photoUrl,
          }
        })

        toastManager.add('Account details updated', { 
          appearance: 'success',
          autoDismiss: true,
        })
      })
      .catch(() => {
        setSubmitting(false)
        toastManager.add('Uh oh! Looks like there was an issue updating your account details. Please try again 🙏', { 
          appearance: 'error',
          autoDismiss: true,
          autoDismissTimeout: 5000,
        })
      })
  }

  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 })

  handleProgress = progress => this.setState({ progress })

  handleUploadError = error => {
    this.setState({ isUploading: false })
    console.error(error)
  }

  handleUploadSuccess = (filename, setFieldValue, setFieldTouched) => {
    this.setState({
      displayPictureFilename: filename,
      progress: 100,
      isUploading: false,
    })

    firebaseService.firebase
      .storage()
      .ref('images/displayPictures')
      .child(filename)
      .getDownloadURL()
      .then(url => this.setState({ displayPictureUrl: url }))
      .then(() => {
        setFieldValue('displayPicture', this.state.displayPictureUrl)
        setFieldTouched('displayPicture', true)
      })
  }

  validate(getValidationSchema) {
    return values => {
      const validationSchema = getValidationSchema(values)
      try {
        validationSchema.validateSync(values, { abortEarly: false })
        return {}
      } catch (error) {
        return this.getErrorsFromValidationError(error)
      }
    }
  }

  getYupValidationSchema() {
    return Yup.object().shape({
      email: Yup.string()
        .email('E-mail is not valid!')
        .required('E-mail is required!'),
    })
  }

  getErrorsFromValidationError(validationError) {
    const FIRST_ERROR = 0
    return validationError.inner.reduce(
      (errors, error) => ({
        ...errors,
        [error.path]: error.errors[FIRST_ERROR],
      }),
      {},
    )
  }

  render() {
    const { displayName, email, photoUrl, uid } = this.props
    const { displayPictureUrl, isUploading } = this.state

    if (!uid) {
      return <Loading />
    }

    return (
      <LayoutContainer padding="large">
        <div className='row' style={{ flex: '1 0 100%' }}>
          <div className="col-xs-12 col-sm-8">
            <Box border style={{ flexFlow: 'column wrap' }}>
              <h1 className='title'>Account Details</h1>
              <Formik
                initialValues={{ email, displayName, photoUrl, newPassword: '' }}
                enableReinitialize
                validate={this.validate(this.getYupValidationSchema)}
                onSubmit={this.onSubmit}
                render={({
                  touched,
                  values,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue,
                  setFieldTouched,
                  isSubmitting,
                  dirty
                }) => (
                  <FormStyled
                    onSubmit={handleSubmit}>
                    <fieldset>
                      <UploadDisplayPicture 
                        displayPicture={displayPictureUrl || photoUrl}
                        handleUploadStart={this.handleUploadStart} 
                        handleUploadError={this.handleUploadError}
                        handleUploadSuccess={this.handleUploadSuccess}
                        handleProgress={this.handleProgress}
                        handleChange={handleChange}
                        isUploading={isUploading}
                        setFieldValue={setFieldValue}
                        setFieldTouched={setFieldTouched}
                      />
                      <Input
                        name="email"
                        label="Email"
                        type="text"
                        placeholder="Enter email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      {touched.email && errors.email && (
                        <Text color="red">{errors.email}</Text>
                      )}
                      <Input
                        name="displayName"
                        label="Display Name"
                        type="text"
                        placeholder="Enter your display name"
                        value={values.displayName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      {touched.displayName && errors.displayName && (
                        <Text color="red">{errors.displayName}</Text>
                      )}

                        
                      
                      <Button className='btn-large' type="submit" disabled={isSubmitting || !dirty} loading={isSubmitting}>Update account details</Button>
                    </fieldset>
                  </FormStyled>
                )}
              />
            </Box>
          </div>
        </div>
      </LayoutContainer>
    )
  }
}

export default withToastManager(Account)