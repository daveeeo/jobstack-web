import * as React from 'react'
import { withRouter } from "react-router";
import firebaseService from '../services/firebaseService'
import JobDescription from '../components/JobDescription'

class JobContainer extends React.Component {
  state = {
    job: null,
    loading: true,
  }

  componentDidMount() {
    const { match } = this.props

    if (match && match.params.objectID) {
      this.fetchJob(match.params.objectID)
    }
  }

  componentDidUpdate(prevProps) {
    const { match } = this.props

    if (match && match.params.objectID !== prevProps.match.params.objectID) {
      match.params.objectID && this.fetchJob(match.params.objectID)
    }
  }

  fetchJob = objectID => {
    const { history } = this.props
    const { store } = firebaseService

    this.setState({ job: null, loading: true })

    store()
      .collection('jobs')
      .doc(objectID)
      .get()
      .then(doc => {
        if (doc.exists) {
          const job = doc.data()
          job.objectID = objectID

          this.setState({
            loading: false,
            job,
          })
        } else {
          history.push('/404')
        }
      })
      .catch(err => console.log(err))
  }

  render() {
    const { loading, job } = this.state
    const { isModal } = this.props

    return (
      <JobDescription job={job} isModal={isModal} loading={loading} {...this.props} />
    )
  }
}

export default withRouter(JobContainer)