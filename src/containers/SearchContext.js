import React, { Component, createContext } from 'react'

const SearchContext = createContext()

const reducer = (state, action) => {
  if (action.type === 'UPDATE_LOCATION_RADIUS') {
    return { ...state, radius: action.newRadius }
  }

  if (action.type === 'UPDATE_SEARCH_CONTEXT') {
    return { ...state, ...action.newContext }
  }

  if (action.type === 'CLEAR_SEARCH_CONTEXT') {
    return { ...state, ...INITIAL_SEARCH_STATE }
  }

  if (action.type === 'ADD_FILTER') {
    const { filters } = state

    filters.push(action.newFilter)
    return { ...state, filters }
  }

  if (action.type === 'REMOVE_FILTER') {
    const { filters } = state

    const filterIndex = filters.findIndex(el => el === action.removeFilter)
    filters.splice(filterIndex, 1)
    return { ...state, filters }
  }
}

const INITIAL_SEARCH_STATE = {
  query: '',
  location: '',
  latlng: null,
  radius: null,
}

class SearchProvider extends Component {
  state = {
    ...INITIAL_SEARCH_STATE,
    filters: [],
  }

  componentDidMount() {
    this.hydrateStateWithLocalStorage()

    // Listen for when user leaves/refreshes page and save state to localStorage
    window.addEventListener('beforeunload', this.saveStateToLocalStorage)
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.saveStateToLocalStorage)

    // Save if component has a chance to unmount
    this.saveStateToLocalStorage()
  }

  dispatch = action => this.setState(state => reducer(state, action))

  hydrateStateWithLocalStorage = () => {
    for (const key in this.state) {
      if (localStorage.hasOwnProperty(key)) {
        let value = localStorage.getItem(key)

        try {
          value = JSON.parse(value)
          this.setState({ [key]: value })
        } catch (err) {
          this.setState({ [key]: value })
        }
      }
    }
  }

  saveStateToLocalStorage = () => {
    for (const key in this.state) {
      localStorage.setItem(key, JSON.stringify(this.state[key]))
    }
  }

  render() {
    const { children } = this.props

    return (
      <SearchContext.Provider
        value={{
          handleLocationUpdate: this.handleLocationUpdate,
          dispatch: this.dispatch,
          ...this.state,
        }}
      >
        {children}
      </SearchContext.Provider>
    )
  }
}

const SearchConsumer = SearchContext.Consumer

export { SearchContext, SearchProvider, SearchConsumer }
