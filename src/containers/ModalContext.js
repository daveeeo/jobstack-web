import React, { createContext } from 'react'
import { Route, Switch } from 'react-router-dom'
import Modal from './Modal';
import Auth from './Auth';
import JobContainer from './JobContainer';

const ModalContext = createContext()

class ModalProvider extends React.Component {

  componentDidUpdate(prevProps, prevState) {
    const { history } = this.props

    // If user presses the back button, change state.isOpen to false 
    if (history.action === 'POP' && prevState.isOpen) {
      this.setState({ isOpen: false })
    }
  }

  onOpenModal = (modalProps) => {
    this.setState({
      isOpen: true,
      modalProps
    })
  }

  onCloseModal = () => {
    const { history } = this.props
    this.setState({ isOpen: false })
    history.goBack()
  }

  state = {
    isOpen: false,
    modalProps: {},
    onOpenModal: this.onOpenModal,
    onCloseModal: this.onCloseModal,
  }

  render() {
    const { children } = this.props
    const {
      isOpen,
      modalProps,
      onOpenModal,
      onCloseModal
    } = this.state

    return (
      <ModalContext.Provider value={this.state}>
        {isOpen ? (
          <Switch>
            <Route
              path='/login'
              exact
              render={(props) => (
                <Modal {...props} isOpen={isOpen} onOpenModal={onOpenModal} onCloseModal={onCloseModal} modalProps={modalProps} component={Auth} />
              )} />
            <Route
              path='/signup'
              exact
              render={(props) => (
                <Modal {...props} isOpen={isOpen} onOpenModal={onOpenModal} onCloseModal={onCloseModal} modalProps={modalProps} component={Auth} />
              )} />
            <Route
              path='/reset-password'
              exact
              render={(props) => (
                <Modal {...props} isOpen={isOpen} onOpenModal={onOpenModal} onCloseModal={onCloseModal} modalProps={modalProps} component={Auth} />
              )} />
            <Route 
              path='/job/:jobSlug/:objectID'
              exact
              render={(props) => (
                <Modal {...props} isOpen={isOpen} onOpenModal={onOpenModal} onCloseModal={onCloseModal} modalProps={modalProps} modalType='modal-job' component={JobContainer} />
              )} />
          </Switch>
        ) : null}

        {children}
      </ModalContext.Provider>
    )
  }
}

const ModalConsumer = ModalContext.Consumer

export { ModalProvider, ModalConsumer }
