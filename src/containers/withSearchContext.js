import React from 'react'
import { SearchConsumer } from './SearchContext'

export default function withSearchContext(Component) {
  return function WrapperComponent(props) {
    return (
      <SearchConsumer>
        {state => <Component {...props} context={state} />}
      </SearchConsumer>
    )
  }
}