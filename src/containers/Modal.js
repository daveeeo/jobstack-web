import React from 'react'
import ModalWrapper from 'react-modal'

ModalWrapper.setAppElement('#root')

const Modal = ({ component: Component, isOpen, onCloseModal, modalType, ...rest }) => (
  <ModalWrapper
    isOpen={isOpen}
    closeTimeoutMS={200}
    contentLabel="modal"
    className={modalType || 'modal'}
    overlayClassName='overlay'
    bodyOpenClassName={modalType === 'modal-job' ? 'modal-job-body' : 'modal-body'}
    onRequestClose={onCloseModal}
  >
    {Component ? <Component isModal onCloseModal={onCloseModal} {...rest} /> : null}
  </ModalWrapper>
)

export default Modal