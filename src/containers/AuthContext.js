import React, { Component } from 'react'
import firebaseService from '../services/firebaseService'

const AuthContext = React.createContext()

const INITIAL_STATE = {
  isAuth: false,
  authStatusReported: false,
  uid: '',
  isAnonymous: true,
  savedJobs: [],
  toggleSaveListing: null,
  activeJobIndex: 0,
}

// Reducers (used to update Context state)

const reducer = (state, action) => {
  if (action.type === 'UPDATE_ACCOUNT_DETAILS') {
    return { ...state, ...action.newAccountDetails }
  }
}

class AuthProvider extends Component {
  constructor() {
    super()
    this.state = INITIAL_STATE

    this.handleSignIn = this.handleSignIn.bind(this)
    this.handleSignOut = this.handleSignOut.bind(this)
    this.signIn = this.signIn.bind(this)
    this.signOut = this.signOut.bind(this)
  }

  componentDidMount() {
    const { auth } = firebaseService

    this.setState({
      dispatch: this.dispatch,
      authStatusReported: false,
      toggleSaveListing: this.toggleSaveListing,
      toggleunreadNotifications: this.toggleunreadNotifications,
      sendPasswordResetEmail: this.sendPasswordResetEmail,
      handleResetPassword: this.handleResetPassword,
      handleVerifyEmail: this.handleVerifyEmail,
      handleVerifyPasswordReset: this.handleVerifyPasswordReset,
      handleConfirmPasswordReset: this.handleConfirmPasswordReset,
      saveActiveJob: this.saveActiveJob
    })

    this.stopAuthListener = auth().onAuthStateChanged(user =>
      user ? this.signIn(user) : this.signOut(),
    )
  }

  componentWillUnmount() {
    this.stopAuthListener()
  }

  dispatch = action => this.setState(state => reducer(state, action))

  handleSignIn = (provider, email, password) => {
    const { auth, firebase } = firebaseService
    const { toastManager } = this.props
    const { isAuth, isAnonymous } = this.state

    switch (provider) {
      case 'google': {
        const credential = new firebase.auth.GoogleAuthProvider()

        if (isAnonymous && isAuth) {
          return this.linkAnonymousUserWithOAuthPopup(credential)
        }

        return this.signInWithOAuthPopup(credential)
      }

      case 'github': {
        const credential = new firebase.auth.GithubAuthProvider()

        if (isAnonymous && isAuth) {
          return this.linkAnonymousUserWithOAuthPopup(credential)
        }

        return this.signInWithOAuthPopup(credential)
      }

      case 'manual':
        return auth()
          .signInWithEmailAndPassword(email, password)
          .catch(this.handleError)

      case 'anonymous':
        return auth()
          .signInAnonymously()
          .then(result => {
            const { user, additionalUserInfo } = result
            additionalUserInfo.isNewUser && this.saveUserToFirestore(user)
            toastManager.add(`This allows you to save jobs and browse the site anonymously until you're ready to signup. Enjoy!`, {
              appearance: 'welcome', 
              heading: 'Welcome to Guest Mode',
            })

            return result
          })
          .catch(this.handleError)

      default: {
        const reason = 'Invalid provider passed to signIn method'
        toastManager.add(`Uh oh! Looks like you've encountered an issue. Please try again 🙏`, { 
          appearance: 'error',
          autoDismiss: true,
          autoDismissTimeout: 5000,
        })
        return Promise.reject(reason)
      }
    }
  }

  handleSignOut = () => {
    const { auth } = firebaseService

    return auth().signOut()
  }

  handleSignUp = async (email, password) => {
    const { firebase, auth } = firebaseService
    const { isAnonymous, isAuth } = this.state
    const { toastManager } = this.props

    if (isAnonymous && isAuth) {
      const credential = await firebase.auth.EmailAuthProvider.credential(email, password)

      return auth()
        .currentUser
        .linkAndRetrieveDataWithCredential(credential)
        .then((result) => {
          const { user } = result
          this.saveUserToFirestore(user)
          this.setState({
            email: user.email,
            isAnonymous: user.isAnonymous,
          })
          toastManager.add('Thanks for joining JobStack!', {
            appearance: 'welcome', 
            autoDismiss: true,
          })
        })
        .catch(this.handleError)
    }

    return auth()
      .createUserWithEmailAndPassword(email, password)
      .then(result => {
        const { user, additionalUserInfo } = result

        if (!user.emailVerified) {
          this.sendEmailVerification(user)
        }
        
        toastManager.add('Thanks for joining JobStack!', {
          appearance: 'welcome', 
          autoDismiss: true,
        })

        additionalUserInfo.isNewUser && this.saveUserToFirestore(user)
      })
      .catch(this.handleError)
  }

  signIn(user) {
    const { uid, isAnonymous } = user
    const { store } = firebaseService
    const { unreadNotifications } = this.state

    this.setState({ 
      isAuth: !!user, 
      isAnonymous,
      uid,
      unreadNotifications,
      toggleSaveListing: this.toggleSaveListing,
      authStatusReported: true,
    })

    const userRef = store()
      .collection('users')
      .doc(uid)
      .get()
      .then(res => {
        if (res.exists) {
          const data = res.data()
          const { email, displayName, photoUrl, unreadNotifications } = data

          return {
            uid,
            isAnonymous,
            email,
            displayName,
            photoUrl,
            unreadNotifications,
          }
        }
        return true
      })

    const eventsRef = store()
      .collection('events')

    // Query Events collection for all savedJobs that belong to this user
    const savedJobsRef = eventsRef
      .where('type', '==', 'savedJob')
      .where('uid', '==', uid)
      .get()
      .then(querySnapshot => {
        const savedJobsArr = []
        querySnapshot.forEach(doc => {
          const data = doc.data()
          const eventID = doc.id

          savedJobsArr.push({
            eventID,
            ...data
          })
        })

        return savedJobsArr
      })

    // After retrieving user data and savedJobs, save them to state 
    Promise.all([userRef, savedJobsRef]).then(([user, savedJobs]) => {
      const { email, displayName, photoUrl, unreadNotifications } = user

      this.setState({
        email,
        displayName,
        photoUrl,
        savedJobs,
        unreadNotifications,
      })
    })
  }

  signOut() {
    this.setState({
      ...INITIAL_STATE,
      authStatusReported: true,
      toggleSaveListing: this.toggleSaveListing,
    })
  }

  signInWithOAuthPopup = (credential) => {
    const { auth } = firebaseService
    const { toastManager } = this.props

    return auth()
      .signInWithPopup(credential)
      .then(result => {
        const { user, additionalUserInfo } = result
        
        if (additionalUserInfo.isNewUser) {
          // Send new user a welcome toast message!
          toastManager.add('Thanks for joining JobStack!', {
            appearance: 'welcome', 
            autoDismiss: true,
          })

          this.saveUserToFirestore(user)
        }
      })
      .catch(this.handleError)
  }

  linkAnonymousUserWithOAuthPopup = async (credential) => {
    const { auth } = firebaseService
    const { toastManager } = this.props

    try {
      const res = await auth()
        .currentUser
        .linkWithPopup(credential);

      const { user } = res;

      this.saveUserToFirestore(user);
      this.setState({
        email: user.email,
        isAnonymous: user.isAnonymous,
        displayName: user.displayName,
        photoUrl: user.photoURL,
        emailVerified: user.emailVerified
      });
      toastManager.add('Thanks for joining JobStack!', {
        appearance: 'welcome', 
        autoDismiss: true,
      })
    } catch(err) {
      this.handleError(err)
    }
  }

  saveUserToFirestore(user) {
    const {
      uid,
      displayName,
      email,
      emailVerified,
      isAnonymous,
      photoUrl,
    } = user
    const { store } = firebaseService

    store()
      .collection('users')
      .doc(uid)
      .set({
        email,
        displayName: displayName || '',
        emailVerified,
        isAnonymous,
        photoUrl: photoUrl || '',
        savedJobs: [],
        unreadNotifications: !!isAnonymous
      })
      .catch(this.handleError)
  }

  toggleSaveListing = async savedJob => {
    const { uid, savedJobs } = this.state

    if (!uid) {
      this.handleSignIn('anonymous', null, null, savedJob)
        .then((res) => this.saveEventToFirestore(savedJob, 'savedJob', 'Job saved to your stack', res.user.uid))
        .then((newEvent) => {
          // Once event has been saved to Firebase, update state with new savedJob
          savedJobs.push(newEvent)
          this.setState({ savedJobs })
        })
    } else {
      // Check to see if the savedJob already exists
      const savedJobIndex = savedJobs.findIndex(el => el.objectID === savedJob.objectID)

      if (savedJobIndex > -1) {
        const { eventID } = savedJobs[savedJobIndex]
        
        // Remove savedJob and update state
        savedJobs.splice(savedJobIndex, 1)
        this.setState({ savedJobs })

        // Remove event from Events collection
        this.removeEventFromFirestore(eventID, 'Job removed from your stack')
      } else {
        const event = await this.saveEventToFirestore(savedJob, 'savedJob', 'Job saved to your stack')

        savedJobs.push(event)
        this.setState({ savedJobs })
      }
    }
  }

  toggleunreadNotifications = () => {
    const { uid, unreadNotifications } = this.state
    const { store } = firebaseService

    if (unreadNotifications) {
      this.setState({ unreadNotifications: false })

      // Set unreadNotifications: true inside store
      store()
        .collection('users')
        .doc(uid)
        .update({
          unreadNotifications: false
        })
        .catch(this.handleError)
    }
  }

  saveEventToFirestore = (event, eventType, toastMessage, uid = this.state.uid) => {
    const { toastManager } = this.props
    const { store } = firebaseService

    // Create reference to Events collection for later use
    const eventsRef = store().collection('events')

    // Create a new docRef in the Events col\lection
    const eventRef = eventsRef.doc()

    const newEvent = {
      eventID: eventRef.id,
      timestamp: Math.floor(Date.now() / 1000),
      uid,
      objectID: event.objectID,
      type: eventType,
      data: {
        ...event
      }
    }

    // Add new savedJob to Events collection. If it fails, revert state back to prevState
    return eventRef.set(newEvent)
      .then(() => {
        toastManager.add(toastMessage, { 
          appearance: 'success',
          autoDismiss: true,
        })

        return newEvent
      })
      .catch(() => {
        toastManager.add('Uh oh! Looks like there was an issue saving this job. Please try to save it again 🙏', { 
          appearance: 'error',
          autoDismiss: true,
          autoDismissTimeout: 5000,
        })
        this.setState(prevState => ({ savedJobs: prevState.savedJobs }))
      })

  }

  removeEventFromFirestore = (eventID, toastMessage) => {
    const { store } = firebaseService
    const { toastManager } = this.props

    // Update events collection by removing this event. If it fails, revert state back to prevState
    store().collection('events')
      .doc(eventID)
      .delete()
      .then(() => toastManager.add(toastMessage, { 
        appearance: 'success', 
        autoDismiss: true,
      }))
      .catch(() => {
        toastManager.add('Uh oh! Looks like there was an issue removing this job. Please try to remove it again 🙏', { 
          appearance: 'error',
          autoDismiss: true,
          autoDismissTimeout: 5000,
        })
        this.setState(prevState => ({ savedJobs: prevState.savedJobs })) // TODO: Need to refactor this so it's not specific to savedJobs
      })
  }

  sendEmailVerification = (user) => {
    const { toastManager } = this.props

    return user.sendEmailVerification()
      .then(() => toastManager.add(`Verification email sent to ${user.email}`, {
        appearance: 'success', 
        autoDismiss: true,
        autoDismissTimeout: 5000,
      }))
      .catch(err => this.handleError(err))
  }

  sendPasswordResetEmail = (email) => {
    const { toastManager } = this.props
    const { auth } = firebaseService

    return auth().sendPasswordResetEmail(email)
      .then(() => toastManager.add(`Reset password verification email sent to ${email}`, {
        appearance: 'success', 
      }))
      .catch(err => this.handleError(err))
  }

  handleVerifyPasswordReset = (actionCode, continueUrl, lang) => {
    const { auth } = firebaseService
    const { toastManager } = this.props

    return auth().verifyPasswordResetCode(actionCode)
      .then((email) => email)
      .catch((err) => {
        toastManager.add(`Uh oh! Looks like we have an issue.`, { 
          appearance: 'error',
        })
        return null
      })
  }

  handleConfirmPasswordReset = (actionCode, email, newPassword) => {
    const { auth } = firebaseService
    const { toastManager } = this.props

    return auth().confirmPasswordReset(actionCode, newPassword)
      .then(() => auth().signInWithEmailAndPassword(email, newPassword))
      .then(() => toastManager.add(`Your password was successfully updated`, {
        appearance: 'success', 
        autoDismiss: true,
        autoDismissTimeout: 5000
      }))
  }

  handleVerifyEmail = (actionCode, continueUrl, lang) => {
    const { auth } = firebaseService
    const { toastManager } = this.props

    return auth().applyActionCode(actionCode)
      .then(() => toastManager.add(`Your account has been successfully verified`, {
        appearance: 'success', 
        autoDismiss: true,
        autoDismissTimeout: 5000
      }))
      .catch(() => toastManager.add(`Looks like you've have already verified your account`, { 
        appearance: 'error',
      }))
  }

  saveActiveJob = (activeJobIndex) => this.setState({ activeJobIndex })

  handleError = (err) => {
    const { toastManager } = this.props

    toastManager.add(err.message, { 
      appearance: 'error',
    })
    
    return null
  }

  render() {
    const { children } = this.props
    const { authStatusReported } = this.state
    return (
      <AuthContext.Provider
        value={{
          ...this.state,
          signIn: this.handleSignIn,
          signUp: this.handleSignUp,
          signOut: this.handleSignOut,
        }}
      >
        {authStatusReported && children}
      </AuthContext.Provider>
    )
  }
}

const AuthConsumer = AuthContext.Consumer
export { AuthContext, AuthProvider, AuthConsumer }
