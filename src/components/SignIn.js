import React, { cloneElement } from 'react'
import styled from 'styled-components/macro'
import media from '../styles/style-media'

const Container = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: transparent;
  cursor: pointer;
  transition: all 200ms;
  padding: 1rem 1.5rem;
  border: 1px solid var(--gray-1);
  border-radius: 4px;

  ${media.phone`
    margin-bottom: 1rem;
  `}

  &:hover {
    color: rgba(0, 0, 0, 0.8);
    border-color: var(--gray-3);
  }

  svg {
    margin-right: 1rem;
    flex-shrink: 0;
  }

  span {
    flex-shrink: 0;
  }
`

const SignIn = ({ onClick, icon, text }) => (
  <Container onClick={onClick} type='button'>
    {icon && cloneElement(icon)}
    <span>{text}</span>
  </Container>
)

export default SignIn
