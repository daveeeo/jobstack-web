/* eslint-disable react/no-danger */
import React from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive';
import styled, { css } from 'styled-components/macro'
import Moment from 'react-moment'
import 'moment-timezone'
import { FiTag as IconTag } from 'react-icons/fi'
import sanitizeHtml from 'sanitize-html'
import SaveListing from './SaveListing'
import { truncate } from '../styles/style-utils'
import JobMeta from './JobMeta'
import withModalContext from '../containers/withModalContext'
import media from '../styles/style-media'

const Container = styled(Link)`
  margin-bottom: 1rem;
  padding: 1rem 1.5rem;
  display: flex;
  flex-direction: column;
  transition: all ease-in-out 0.18s;
  background-color: white;
  border: 1px solid var(--gray-1);
  cursor: pointer;
  color: inherit;

  ${media.tablet`
    margin-bottom: 1.5rem;
  `};

  &:hover {
    box-shadow: var(--box-shadow-container-light);
  }

  ${props =>
    props.isActive &&
    css`
      border-radius: 8px 8px 0 0;
      border-color: transparent;

      &:hover {
        box-shadow: none;
      }
    `};
`

const JobMetaStyled = styled(JobMeta)`
  .date-posted {
      display: none;

      ${media.tablet`
        display: inherit; 
      `}
    }
`

const Label = styled.span`
  background-color: white;
  border: 1px solid var(--gray-1);
  padding: 0.4rem 0.5rem;
  margin-right: 0.5rem;
  text-align: center;
  border-radius: 6px;
  display: inline-block;
  font-size: var(--text-sm);
`

const MainDetails = styled.div`
  flex-grow: 1;
  border-bottom: 1px solid var(--gray-1);
  margin-bottom: 1rem;
  h1 {
    font-size: 1.35rem;
    font-weight: 700;
    margin: 1rem 0;

    ${media.tablet`
      ${truncate('560px')};
    `}
    
    a {
      color: inherit;
    }
  }

  > div {
    display: block;
    display: -webkit-box;
    max-width: 100%;
    height: 44px;
    margin: 0 0 1rem;
    font-size: 0.95rem;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: var(--body-line-height);

    div {
      line-height: var(--body-line-height);
    }

    p,
    ul {
      margin: 0;
    }

    ul {
      list-style-type: none;
      padding: 0;
    }
  }

  strong,
  b {
    display: none;
  }
`

const AdditionalDetails = styled.div`
  display: flex;
  align-items: center;
  font-size: var(--text-sm);
  text-align: right;

  > div {
    display: flex;
    align-items: center;
  }
`

const Listing = ({ hit, context }) => {
  const description = `${sanitizeHtml(hit.description, {
    allowedTags: [],
    allowedAttributes: []
  }).substring(0, 200)}...`

  let windowWidth; let maxCategoriesDisplayed

  if (!windowWidth && typeof window !== `undefined`) {
    windowWidth = window.innerWidth
  }
  
  // Set max number of categories to be displayed based on windowWidth
  if (windowWidth < 500) {
    maxCategoriesDisplayed = 3
  } else if (windowWidth < 600) {
    maxCategoriesDisplayed = 4
  } else {
    maxCategoriesDisplayed = 5
  }

  return (
    <MediaQuery minWidth={1170}>
      {(matches) => (
        <Container
          to={{
            pathname: `/job/${hit.slug}/${hit.objectID}`,
            state: { modal: matches && true }
          }}
          onClick={() => matches && context.onOpenModal({ objectID: hit.objectID })}
        >
          <JobMetaStyled
            logo={hit.companyLogo}
            companyName={hit.companyName}
            jobLocation={hit.jobLocation}
            remoteFriendly={hit.remoteFriendly}
            datePosted={hit.datePosted}
          />
          <MainDetails>
            <h1 dangerouslySetInnerHTML={{
                __html: hit.title,
              }} />
            {hit.description && (
              <div
                dangerouslySetInnerHTML={{
                  __html: description,
                }}
              />
            )}
          </MainDetails>
          <AdditionalDetails>
            <SaveListing
              job={hit}
              showLabel
              style={{ marginRight: '1.5rem' }}
            />
            <div className='hidden-phone'>
              {hit.categories && (
                <>
                  <IconTag
                    style={{
                      width: '18px',
                      height: '18px',
                      stroke: 'var(--gray-3)',
                      marginRight: '0.75rem',
                    }}
                  />
                  {hit.categories
                    .slice(0, maxCategoriesDisplayed)
                    .map(category => (
                      <Label key={category}>{category}</Label>
                    ))}
                  {hit.categories.length > maxCategoriesDisplayed && (
                    <span>+{hit.categories.length - maxCategoriesDisplayed}</span>
                  )}
                </>
              )}
            </div>
            <div className='hidden-tablet'>
              <span className="date-posted">
              Posted&nbsp;
                <Moment
                  unix
                  fromNowDuring={172800000}
                  tz="Australia/Sydney"
                  format="MMM D"
                >
                  {hit.datePosted}
                </Moment>
              </span>
            </div>
            
          </AdditionalDetails>
        </Container>
      )}
    </MediaQuery>
  )
}

export default withModalContext(Listing)