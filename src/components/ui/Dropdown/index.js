import React, { Component } from 'react'
import styled, { css } from 'styled-components/macro'
import { ChevronDown } from '../Icons'
import Button from '../Elements/Button';

const DropdownWrapper = styled.div`
  display: inline-flex;
  position: relative;
  height: 100%;

  .dropdown-menu {
    display: none;
    left: 0;
    min-width: 12rem;
    position: absolute;
    top: 100%;
    z-index: 2;
    ${props =>
    props.align === 'right'
      ? css`
            ${alignDropdownRight};
          `
      : ''};

    .dropdown-content {
      background-color: #fff;
      border: 1px solid var(--gray-1);
      padding-bottom: 0.75rem;
      padding-top: 0.75rem;
      box-shadow: var(--box-shadow-container);

      > a {
        color: #4a4a4a;
        display: block;
        font-size: 0.875rem;
        line-height: 1.5;
        padding: 0.75rem 1.5rem;
        position: relative;
        text-align: left;
        white-space: nowrap;
        width: 100%;

        &:hover {
          background-color: #f5f5f5;
          color: #0a0a0a;
        }
      }

      input {
        margin: 0.25rem 0.7rem;
        padding: 0.5rem;
        border: 1px solid var(--gray-1);
        border-radius: 4px;
      }

      ul {
        list-style: none;
        padding: 0;
        margin: 0;
        li {
          padding: 0.4rem 1rem;

          &:hover {
            background: #f7f7f7;
          }

          div {
            cursor: pointer;
          }
        }
      }

      div {
        padding: 0.75rem 1.5rem;
        display: flex;
        flex-flow: column wrap;

        p {
          margin: 0;
          min-width: 270px;
        }
      }
    }
  }

  ${props =>
    props.isHoverable &&
    css`
      &:hover {
        .dropdown-menu {
          display: block;
        }
      }
    `};

  ${props =>
    props.isActive &&
    css`
      .dropdown-menu {
        display: block;
      }
    `};
`

const alignDropdownRight = `
  left: auto;
  right: -1px;
`

const IconWrapper = styled.span`
  position: relative;
  top: 1px;
  margin-left: 0.5rem;
`

const Label = styled.span`
  display: inline-flex;
`

class Dropdown extends Component {
  constructor(props) {
    super(props)
    this.containerRef = React.createRef()
    this.dropdownMenuRef = React.createRef()
    this.state = {
      isActive: false,
    }
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside, false)
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside, false)
  }

  handleToggle = () => {
    const { isActive } = this.state
    this.setState({ isActive: !isActive })
  }

  handleClose = () => {
    this.setState({ isActive: false })
  }

  handleClickOutside = e => {
    const containerNode = this.containerRef.current

    if (containerNode && !containerNode.contains(e.target)) {
      this.handleClose()
    }
  }

  render() {
    const {
      textLabel,
      componentLabel,
      children,
      align,
      showArrow = true,
      isHoverable,
      buttonStyles,
      buttonClass
    } = this.props

    const { isActive } = this.state

    return (
      <DropdownWrapper
        className="dropdown-wrapper"
        align={align}
        isHoverable={isHoverable}
        isActive={isActive}
        ref={this.containerRef}
      >
        <Button
          className={`btn-empty dropdown-button ${buttonClass}`}
          type='button'
          onClick={this.handleToggle}
          style={buttonStyles}
        >
          {textLabel && <Label>{textLabel}</Label>}
          {componentLabel && <Label>{componentLabel}</Label>}
          {showArrow && (
            <IconWrapper className="icon is-small" style={{ marginLeft: '1rem' }}>
              <ChevronDown />
            </IconWrapper>
          )}
        </Button>
        <div
          className="dropdown-menu"
          id="dropdown-menu"
          role="menu"
          ref={this.dropdownMenuRef}
        >
          <div className="dropdown-content">{children}</div>
        </div>
      </DropdownWrapper>
    )
  }
}

export default Dropdown
