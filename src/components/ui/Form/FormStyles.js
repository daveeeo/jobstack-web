import styled from 'styled-components/macro'
import media from '../../../styles/style-media'

export const FormStyled = styled.form`
  width: 100%;
  min-width: 254px;
  
  ${media.tablet`
    max-width: 440px;
  `}

  fieldset {
    padding: 0;
    border: 0;

    h2, h3 {
      margin: 1rem 0 2rem;
      text-align: center;
    }
  }
`

export const Field = styled.div`
  clear: both;
  margin: 0 0 1.5rem;
  position: relative;
`

export const Label = styled.label`
  display: block;
  font-size: 1rem;
  font-weight: 700;
  padding-bottom: 0.75rem;
  transition: all 0.2s;
`

export const Text = styled.p`
  color: red;
`
