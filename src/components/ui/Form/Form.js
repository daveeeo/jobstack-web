import React from 'react'
import styled from 'styled-components/macro'

const Form = ({ children, onSubmit, className, type }) => (
  <form className={className} onSubmit={onSubmit}>
    {type && { children }}
  </form>
)

export default Form
