import React from 'react'
import { Label } from './FormStyles'

const SelectComponent = ({
  name,
  label,
  value,
  onChange,
  onBlur,
  required,
  hideLabel,
  children,
  className,
}) => (
  <>
    <Label htmlFor={name} className={`label ${hideLabel ? 'sr-only' : ''}`}>
      {label}{' '}
      {required && (
        <abbr className="abbr" title="required">
          *
        </abbr>
      )}
    </Label>

    <select
      id={name}
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      className={className}
      required={required}
    >
      {children}
    </select>
  </>
)

export default SelectComponent
