import React from 'react'
import styled from 'styled-components/macro'
import { Field, Label } from './FormStyles'

const Textarea = styled.textarea`
  -moz-appearance: none;
  -webkit-appearance: none;
  align-items: center;
  border-radius: 4px;
  box-shadow: none;
  display: inline-flex;
  justify-content: flex-start;
  padding: 1rem;
  position: relative;
  vertical-align: top;
  background-color: white;
  border: 1px solid var(--gray-1);
  box-shadow: none;
  max-width: 100%;
  width: 100%;
  transition: all 0.2s;
  &[disabled] {
    cursor: not-allowed;
  }
  &::-moz-placeholder,
  &::-webkit-input-placeholder,
  &:-moz-placeholder,
  &:-ms-input-placeholder {
    color: var(--gray-1);
  }
  &:hover {
    border-color: #b5b5b5;
  }
  &:focus {
    outline: none;
    border-color: #3273dc;
    box-shadow: 0 0 0 0.125em rgba(50, 115, 220, 0.25);
  }
`

const TextareaComponent = ({
  name,
  label,
  type,
  value,
  placeholder,
  onChange,
  onBlur,
  required,
}) => (
  <Field>
    <Label htmlFor={name} className="label">
      {label}{' '}
      {required && (
        <abbr className="abbr" title="required">
          *
        </abbr>
      )}
    </Label>

    <Textarea
      id={name}
      type={type}
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      onBlur={onBlur}
      required={required}
    />
  </Field>
)

export default TextareaComponent
