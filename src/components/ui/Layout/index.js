import styled from 'styled-components/macro'
import media from '../../../styles/style-media'

const LayoutContainer = styled.div`
  display: flex;
  padding: 1rem;
  width: 100%;

  ${media.tablet`
    padding: 1.5rem;
    ${props => props.padding === 'xsmall' && `padding: 0.5rem`}
    ${props => props.padding === 'small' && `padding: 1rem`}
    ${props => props.padding === 'medium' && `padding: 1.5rem`}
    ${props => props.padding === 'large' && `padding: 2rem`}
    ${props => props.padding === 'xlarge' && `padding: 3rem`}
  `}
`

export default LayoutContainer
