import React from 'react'

export const Google = props => (
  <svg {...props} width="18" height="18" xmlns="http://www.w3.org/2000/svg">
    <g fill="none" fillRule="evenodd">
      <path
        d="M17.64 9.205c0-.639-.057-1.252-.164-1.841H9v3.481h4.844a4.14 4.14 0 0 1-1.796 2.716v2.259h2.908c1.702-1.567 2.684-3.875 2.684-6.615z"
        fill="#4285F4"
      />
      <path
        d="M9 18c2.43 0 4.467-.806 5.956-2.18l-2.908-2.259c-.806.54-1.837.86-3.048.86-2.344 0-4.328-1.584-5.036-3.711H.957v2.332A8.997 8.997 0 0 0 9 18z"
        fill="#34A853"
      />
      <path
        d="M3.964 10.71A5.41 5.41 0 0 1 3.682 9c0-.593.102-1.17.282-1.71V4.958H.957A8.996 8.996 0 0 0 0 9c0 1.452.348 2.827.957 4.042l3.007-2.332z"
        fill="#FBBC05"
      />
      <path
        d="M9 3.58c1.321 0 2.508.454 3.44 1.345l2.582-2.58C13.463.891 11.426 0 9 0A8.997 8.997 0 0 0 .957 4.958L3.964 7.29C4.672 5.163 6.656 3.58 9 3.58z"
        fill="#EA4335"
      />
      <path d="M0 0h18v18H0z" />
    </g>
  </svg>
)

export const Business = props => (
  <svg
    {...props}
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 1024 1024"
  >
    <path className="path1" d="M716.8 307.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path2" d="M716.8 409.6h51.2v51.2h-51.2v-51.2z" />
    <path className="path3" d="M716.8 512h51.2v51.2h-51.2v-51.2z" />
    <path className="path4" d="M716.8 614.4h51.2v51.2h-51.2v-51.2z" />
    <path className="path5" d="M716.8 819.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path6" d="M716.8 716.8h51.2v51.2h-51.2v-51.2z" />
    <path className="path7" d="M307.2 307.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path8" d="M307.2 409.6h51.2v51.2h-51.2v-51.2z" />
    <path className="path9" d="M307.2 512h51.2v51.2h-51.2v-51.2z" />
    <path className="path10" d="M307.2 614.4h51.2v51.2h-51.2v-51.2z" />
    <path className="path11" d="M307.2 819.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path12" d="M307.2 716.8h51.2v51.2h-51.2v-51.2z" />
    <path className="path13" d="M204.8 307.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path14" d="M204.8 409.6h51.2v51.2h-51.2v-51.2z" />
    <path className="path15" d="M204.8 512h51.2v51.2h-51.2v-51.2z" />
    <path className="path16" d="M204.8 614.4h51.2v51.2h-51.2v-51.2z" />
    <path className="path17" d="M204.8 819.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path18" d="M204.8 716.8h51.2v51.2h-51.2v-51.2z" />
    <path className="path19" d="M409.6 307.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path20" d="M409.6 409.6h51.2v51.2h-51.2v-51.2z" />
    <path className="path21" d="M409.6 512h51.2v51.2h-51.2v-51.2z" />
    <path className="path22" d="M409.6 614.4h51.2v51.2h-51.2v-51.2z" />
    <path className="path23" d="M409.6 819.2h51.2v51.2h-51.2v-51.2z" />
    <path className="path24" d="M409.6 716.8h51.2v51.2h-51.2v-51.2z" />
    <path
      className="path25"
      d="M947.2 972.8h-25.6v-691.2c0-39.058-29.024-79.326-66.077-91.677l-241.123-80.374v-83.949c0-8.093-3.827-15.709-10.318-20.539-6.493-4.829-14.888-6.306-22.637-3.981l-462.96 138.886c-37.73 11.32-67.285 51.043-67.285 90.434v742.4h-25.6c-14.138 0-25.6 11.461-25.6 25.6s11.462 25.6 25.6 25.6h921.6c14.139 0 25.6-11.461 25.6-25.6s-11.461-25.6-25.6-25.6zM839.333 238.496c16.259 5.419 31.067 25.965 31.067 43.104v691.2h-256v-809.282l224.933 74.978zM102.4 230.4c0-16.827 14.678-36.557 30.797-41.392l430.003-129.002v912.794h-460.8v-742.4z"
    />
  </svg>
)

export const Layers = props => (
  <svg 
    xmlns="http://www.w3.org/2000/svg" 
    width="512" 
    height="512" 
    viewBox="0 0 512 512" 
    fill="#ffffff"
    stroke="#000000"
    strokeWidth="28px"
    fillRule='evenodd' 
    {...props}
  >
    <path data-name="Rounded Rectangle 1" className="cls-1" d="M25.652,310.169L222.686,202.4c20.532-11.231,53.663-11.042,74,.422L491.846,312.827c20.337,11.464,20.179,29.861-.353,41.091L294.459,461.69c-20.532,11.23-53.663,11.041-74-.422L25.3,351.259C4.962,339.8,5.121,321.4,25.652,310.169Z"/>
    <path data-name="Rounded Rectangle 1 copy" className="cls-1" d="M23.073,231.125L220.107,123.353c20.532-11.231,53.662-11.042,74,.422L489.267,233.784c20.336,11.463,20.178,29.86-.354,41.09L291.879,382.646c-20.532,11.231-53.662,11.042-74-.422L22.719,272.216C2.383,260.752,2.541,242.355,23.073,231.125Z"/>
    <path data-name="Rounded Rectangle 1 copy 2" className="cls-1" d="M20.493,152.081L217.527,44.309c20.532-11.23,53.663-11.041,74,.422L486.687,154.74c20.337,11.463,20.179,29.86-.353,41.091L289.3,303.6c-20.532,11.23-53.663,11.041-74-.422L20.14,193.172C-0.2,181.708-.039,163.311,20.493,152.081Z"/>
  </svg>
)

export const Code = props => (
  <svg
    {...props}
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 1024 1024"
  >
    <path
      className="path1"
      d="M256 768c-6.552 0-13.102-2.499-18.101-7.499l-204.8-204.8c-9.998-9.997-9.998-26.206 0-36.203l204.8-204.8c9.997-9.997 26.206-9.997 36.203 0 9.998 9.997 9.998 26.206 0 36.203l-186.699 186.699 186.698 186.699c9.998 9.997 9.998 26.206 0 36.203-4.998 4.998-11.549 7.498-18.101 7.498z"
    />
    <path
      className="path2"
      d="M768 768c-6.552 0-13.102-2.499-18.101-7.499-9.998-9.997-9.998-26.206 0-36.203l186.698-186.698-186.698-186.699c-9.998-9.997-9.998-26.206 0-36.203 9.997-9.997 26.206-9.997 36.203 0l204.8 204.8c9.998 9.997 9.998 26.206 0 36.203l-204.8 204.8c-5 5-11.55 7.499-18.102 7.499z"
    />
    <path
      className="path3"
      d="M383.976 768.003c-4.634 0-9.325-1.258-13.544-3.894-11.989-7.494-15.634-23.288-8.141-35.278l256-409.6c7.493-11.984 23.283-15.634 35.278-8.141 11.989 7.494 15.634 23.288 8.141 35.278l-256 409.6c-4.858 7.77-13.202 12.035-21.734 12.035z"
    />
  </svg>
)

export const ChevronDown = props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    {...props}
  >
    <polyline points="6 9 12 15 18 9" />
  </svg>
)

export const Search = props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    {...props}
  >
    <circle cx="11" cy="11" r="8" />
    <line x1="21" y1="21" x2="16.65" y2="16.65" />
  </svg>
)

export const Location = props => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="20"
    height="20"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    {...props}
  >
    <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z" />
    <circle cx="12" cy="10" r="3" />
  </svg>
)

export const Listings = props => (
  <svg
    version="1.1"
    id="Capa_1"
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    width="511.626px"
    height="511.627px"
    viewBox="0 0 511.626 511.627"
    style={{ enableBackground: 'new 0 0 511.626 511.627' }}
    {...props}
  >
    <g>
      <g>
        <path
          d="M498.208,68.235c-8.945-8.947-19.701-13.418-32.261-13.418H45.682c-12.562,0-23.318,4.471-32.264,13.418
			C4.471,77.184,0,87.935,0,100.499v310.633c0,12.566,4.471,23.312,13.418,32.265c8.945,8.945,19.701,13.414,32.264,13.414h420.266
			c12.56,0,23.315-4.469,32.261-13.414c8.949-8.953,13.418-19.705,13.418-32.265V100.499
			C511.626,87.935,507.158,77.18,498.208,68.235z M475.078,411.125c0,2.475-0.903,4.616-2.71,6.424
			c-1.804,1.81-3.949,2.706-6.42,2.706H45.682c-2.474,0-4.615-0.896-6.423-2.706c-1.809-1.808-2.712-3.949-2.712-6.424V173.588
			c0-2.475,0.903-4.617,2.712-6.427c1.809-1.806,3.949-2.709,6.423-2.709h420.266c2.471,0,4.613,0.9,6.42,2.709
			c1.807,1.81,2.71,3.952,2.71,6.427V411.125L475.078,411.125z"
        />
        <path
          d="M100.5,347.179H82.228c-2.474,0-4.615,0.896-6.423,2.703c-1.807,1.811-2.712,3.953-2.712,6.427v18.271
			c0,2.478,0.905,4.616,2.712,6.427c1.809,1.81,3.949,2.707,6.423,2.707H100.5c2.473,0,4.615-0.897,6.423-2.707
			c1.807-1.811,2.712-3.949,2.712-6.427v-18.271c0-2.474-0.905-4.616-2.712-6.427C105.115,348.082,102.973,347.179,100.5,347.179z"
        />
        <path
          d="M100.5,274.081H82.228c-2.474,0-4.615,0.91-6.423,2.714c-1.807,1.811-2.712,3.953-2.712,6.424v18.273
			c0,2.479,0.905,4.61,2.712,6.428c1.809,1.8,3.949,2.706,6.423,2.706H100.5c2.473,0,4.615-0.906,6.423-2.706
			c1.807-1.817,2.712-3.949,2.712-6.428v-18.273c0-2.471-0.905-4.62-2.712-6.424C105.115,274.991,102.973,274.081,100.5,274.081z"
        />
        <path
          d="M100.5,200.998H82.228c-2.474,0-4.615,0.902-6.423,2.708c-1.807,1.812-2.712,3.949-2.712,6.423v18.276
			c0,2.473,0.905,4.615,2.712,6.424c1.809,1.803,3.949,2.712,6.423,2.712H100.5c2.473,0,4.615-0.905,6.423-2.712
			c1.807-1.809,2.712-3.951,2.712-6.424v-18.276c0-2.474-0.905-4.615-2.712-6.423C105.115,201.902,102.973,200.998,100.5,200.998z"
        />
        <path
          d="M429.399,347.179H155.313c-2.473,0-4.615,0.896-6.423,2.703c-1.807,1.811-2.712,3.953-2.712,6.427v18.271
			c0,2.478,0.905,4.616,2.712,6.427c1.809,1.81,3.951,2.707,6.423,2.707h274.089c2.479,0,4.617-0.897,6.428-2.707
			c1.803-1.811,2.707-3.949,2.707-6.427v-18.271c0-2.474-0.907-4.616-2.711-6.427C434.019,348.075,431.876,347.179,429.399,347.179z
			"
        />
        <path
          d="M429.399,274.081H155.313c-2.473,0-4.615,0.91-6.423,2.714c-1.807,1.811-2.712,3.953-2.712,6.424v18.273
			c0,2.479,0.905,4.61,2.712,6.428c1.809,1.8,3.951,2.706,6.423,2.706h274.089c2.479,0,4.617-0.906,6.428-2.706
			c1.803-1.817,2.707-3.949,2.707-6.428v-18.273c0-2.471-0.907-4.62-2.711-6.424C434.019,274.991,431.876,274.081,429.399,274.081z"
        />
        <path
          d="M429.399,200.998H155.313c-2.473,0-4.615,0.902-6.423,2.708c-1.807,1.812-2.712,3.949-2.712,6.423v18.276
			c0,2.473,0.905,4.619,2.712,6.424c1.809,1.803,3.951,2.712,6.423,2.712h274.089c2.479,0,4.624-0.905,6.428-2.712
			c1.803-1.809,2.707-3.951,2.707-6.424v-18.276c0-2.474-0.907-4.615-2.711-6.423C434.019,201.902,431.876,200.998,429.399,200.998z
			"
        />
      </g>
    </g>
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
  </svg>
)

export const LoadingSpinner = props => (
  <svg className="loading-spinner" width="32px"  height="32px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" {...props}><g transform="rotate(0 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.9166666666666666s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(30 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.8333333333333334s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(60 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(90 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.6666666666666666s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(120 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5833333333333334s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(150 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(180 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.4166666666666667s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(210 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.3333333333333333s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(240 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(270 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.16666666666666666s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(300 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.08333333333333333s" repeatCount="indefinite"></animate>
    </rect>
  </g><g transform="rotate(330 50 50)">
    <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#ffffff">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>
    </rect>
  </g></svg>
)