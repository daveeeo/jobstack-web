import styled from 'styled-components/macro'
import media from '../../../styles/style-media'

const Box = styled.div`
  display: flex;
  background-color: white;
  border: ${props => props.border && '1px solid var(--gray-1)'};
  box-shadow: ${props => props.boxShadow && '0 2px 4px 0 rgba(50, 50, 93, 0.1)'};
  padding: 1rem;
  margin-bottom: 1.5rem;

  ${media.tablet`
    padding: 1.5rem;
  `};

  h1 {
    font-size: 1.45rem;
    margin: 0.5rem 0 2rem;

    ${media.tablet`
      font-size: 1.85rem;
    `}
  }

  h2 {
    text-transform: uppercase;
    font-size: 1.25rem;
    margin-bottom: 1rem;
  }

  h3 {
    text-transform: uppercase;
    font-size: 0.95rem;
    margin-bottom: 1rem;
  }
`

export default Box