import React from 'react'
import styled from 'styled-components/macro'
import { LoadingSpinner } from '../Icons';

const ButtonStyles = styled.button`
  cursor: pointer;
  justify-content: center;
  padding: 0.5rem 1rem;
  text-align: center;
  white-space: nowrap;
  position: relative;
  vertical-align: top;
  display: inline-flex;
  -moz-appearance: none;
  -webkit-appearance: none;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  align-items: center;
  background-color: var(--color-primary);
  border-color: transparent;
  color: #fff;
  border: none;
  border-radius: 4px;
  box-shadow: none;
  transition: all 200ms;

  &.btn-empty {
    background: none;
    color: var(--color-text);
    border-radius: 0;

    &:hover {
      background: none;
      color: var(--color-text-heading);
    }
  }

  &.btn-large {
    padding: 1.2rem 1.5rem;
  }

  &.btn-fullwidth {
    width: 100%;
  }

  &:focus {
    outline: none;
  }

  &:hover {
    background-color: var(--color-primary-dark);
    /* border-color: transparent; */
    color: #fff;
  }

  &:visited {
    color: white;
  }

  &:disabled,
  &:disabled:hover {
    background-color: var(--color-primary);
    opacity: .65;
    cursor: unset;
  }

  .label {
    display: flex;
    opacity: ${props => props.loading ? 0 : 1};
  }

  .loading-spinner {
    position: absolute;
    opacity: ${props => props.loading ? 1 : 0};
  }
`

const Button = ({ className, onClick, loading, disabled, children, type, style, as, href, target, role, ariaLabel }) => (
  <ButtonStyles 
    className={className} 
    type={type} 
    onClick={onClick} 
    disabled={disabled} 
    loading={loading} 
    style={style} 
    as={as} 
    href={href} 
    target={target}
    role={role}
    aria-label={ariaLabel}
  >
    <span className='label'>{children}</span>
    {loading && <LoadingSpinner />}
  </ButtonStyles>
)

export default Button