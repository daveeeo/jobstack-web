import React from 'react'
import styled, { css } from 'styled-components/macro'
import { TransitionGroup } from "react-transition-group";
import { FiX as CloseIcon, FiCheck as SuccessIcon } from "react-icons/fi";
import { MdPriorityHigh as AlertIcon } from "react-icons/md";
import { IoMdHand as WelcomeIcon } from "react-icons/io";
import media from '../../../styles/style-media'

const placements = {
  'top-left': { top: 0, left: 0 },
  'top-center': { top: 0, left: '50%', transform: 'translateX(-50%)' },
  'top-right': { top: 0, right: 0 },
  'bottom-left': { bottom: 0, left: 0 },
  'bottom-center': { bottom: 0, left: '50%', transform: 'translateX(-50%)' },
  'bottom-right': { bottom: 0, right: 0 },
};


const animationStates = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};

const ToastContainerStyles = styled.div(props => css`
  box-sizing: border-box;
  max-height: 100%;
  overflow: initial;
  pointer-events: auto;
  position: fixed;
  z-index: 110;
  width: 100%;
  ${{...placements[props.placement]}};

  ${media.tablet`
    width: auto;
  `}
`)

const ToastStyles = styled.div(props => css`
  background-color: rgba(255,255,255,0.95);
  border-left: 6px solid;
  border-color: ${props.appearance === 'success' && 'var(--color-success)'};
  border-color: ${props.appearance === 'warning' && 'var(--color-warning)'};
  border-color: ${props.appearance === 'error' && 'var(--color-error)'};
  border-color: ${props.appearance === 'welcome' && 'var(--color-primary)'};
  padding: 0;
  border-radius: 4px;
  box-shadow: 0 5px 15px rgba(0,0,0,.05), 0 -2px 60px rgba(0,0,0,.05);
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  pointer-events: initial;
  transition: all 220ms ease-in-out;
  z-index: 2;
  opacity: 1;
  margin: 1rem;
  ${{...animationStates[props.transitionState]}}

  ${media.tablet`
    width: 400px;
    margin: 1.5rem 2rem 1rem 0;
  `}

  .toast-icon {
    display: inline-flex;
    margin: 1.5rem;
    border-radius: 50%;
    width: 28px;
    height: 28px;
    justify-content: center;
    align-items: center;
    background: ${props.appearance === 'success' && 'var(--color-success)'};
    background: ${props.appearance === 'warning' && 'var(--color-warning)'};
    background: ${props.appearance === 'error' && 'var(--color-error)'};
    background: ${props.appearance === 'welcome' && 'var(--color-primary)'};
    svg {
      ${props.appearance === 'success' && 'stroke: white'};
      ${props.appearance === 'warning' && 'fill: white'};
      ${props.appearance === 'error' && 'fill: white'};
      ${props.appearance === 'welcome' && 'fill: white'};
    }
  }

  .toast-content {
    flex: 1;
    padding: 1.5rem 0;
    font-size: 0.95rem;
    line-height: 1.35;

    h4 {
      margin: 0 0 0.2rem 0;
    }

    a {
      color: var(--color-primary);
      
      &:hover {
        text-decoration: underline;
      }
    }
  }

  button {
    display: inline-flex;
    background: none;
    border: none;
    padding: 1.5rem;
    cursor: pointer;

    &:focus {
      outline: none;
    }
  }
`)

const ToastContainer = ({ children, placement }) => (
  <ToastContainerStyles placement={placement}>
    <TransitionGroup component={null}>{children}</TransitionGroup>
  </ToastContainerStyles>
)

const Toast = ({ children, appearance, onDismiss, transitionDuration, transitionState, heading }) => (
  <ToastStyles transitionDuration={transitionDuration} transitionState={transitionState} appearance={appearance}>
    {appearance === 'success' && <span className='toast-icon'><SuccessIcon /></span>}
    {appearance === 'warning' && <span className='toast-icon'><AlertIcon /></span>}
    {appearance === 'error' && <span className='toast-icon'><AlertIcon /></span>}
    {appearance === 'welcome' && <span className='toast-icon'><WelcomeIcon /></span>}
    <div className='toast-content'>
      {heading && <h4>{heading}</h4>}
      {children}
    </div>
    <button
      onClick={onDismiss}
      type="button"
    >
      <CloseIcon />
    </button>
  </ToastStyles>
)

export { ToastContainer, Toast }