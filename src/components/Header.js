import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components/macro'
import media from '../styles/style-media'

import Search from './Search'
import Nav from './Nav'
import { Layers } from './ui/Icons'

const Logo = styled.h1`
  font-size: 1.2rem;
  font-weight: 700;
  display: flex;
  align-items: center;
  margin: 0;
  padding: 0;
  font-family: var(--font-logo);
  border-bottom: 1px solid var(--gray-1);

  ${media.desktop`
    border-right: 1px solid var(--gray-1);
    border-bottom: none;
  `};

  a {
    color: black;
    text-decoration: none;
    padding: 0.5rem 1rem;
    display: flex;

    ${media.desktop`
      padding: 1.5rem;
    `};

    svg {
      width: 24px;
      height: 22px;
      stroke: var(--color-primary);
      margin-top: 1px;
      margin-right: 0.5rem;
    }
  }
`

const Container = styled.div`
  top: 0;
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;

  ${media.desktop`
    flex-wrap: nowrap;
  `};
`

const StyledHeader = styled.header`
  background-color: white;
  position: relative;
  z-index: 2;
  height: ${props => props.isHome ? 'var(--header-height-phone-withsearch)' : 'var(--header-height-phone)' };
  
  ${media.desktop`
    height: var(--header-height);
    border-bottom: 1px solid var(--gray-1);
  `};
`
const Header = ({ location }) => (
  <StyledHeader id='header-main' isHome={ location.pathname === '/' }>
    <Container>
      <Logo>
        <Link to="/" aria-label='Home'>
          <Layers /> jobstack.
        </Link>
      </Logo>
      {(location.pathname === '/' || location.pathname.includes('/job/')) && <Search isHidden={location.pathname !== '/'} />}
      {/* <Search /> */}
      {/* {location.pathname === '/saved' && (
        <div>
          <h3>Saved Jobs</h3>
        </div>
      )} */}
      <Nav />
    </Container>
  </StyledHeader>
)

export default Header
