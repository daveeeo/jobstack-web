import React from 'react'
import styled from 'styled-components/macro'
import Moment from 'react-moment'
import 'moment-timezone'
import media from '../styles/style-media'
import CompanyLogo from './CompanyLogo'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
  font-size: 0.9rem;

  .date-posted {
    display: flex;
    align-items: center;
  }
`

const CompanyLogoStyled = styled(CompanyLogo)`
  display: none;

  ${media.tablet`
    display: flex;
  `};
`

const JobMeta = ({
  logo,
  companyName,
  jobLocation,
  remoteFriendly,
  datePosted,
  isModal,
  hideLogo,
  style,
  className
}) => {

  // Format location based on whether it's remote or not
  let location

  if (remoteFriendly) {
    location = 'Remote'
  } else if (jobLocation) {
    const city = Array.isArray(jobLocation) ? jobLocation[0].city : jobLocation.city
    const country = Array.isArray(jobLocation) ? jobLocation[0].country : jobLocation.country
    location = `${city}${city && ','} ${country}`
  }

  return (
    <Container isModal={isModal} className={`job-meta ${className}`} style={style}>
      <CompanyLogoStyled
        style={{ width: '32px', height: '32px', display: hideLogo && 'none' }}
        logo={logo && logo}
        companyName={companyName}
      />
      {companyName && <span className="company-name">{companyName}</span>}
      {location && (
        <>
          <span className="middot" style={{ display: hideLogo && 'none' }} />
          <span className="location">{location}</span>
        </>
      )}
      {datePosted && (
        <span className='date-posted'>
          <span className="middot" />
          <Moment
            unix
            fromNowDuring={172800000}
            tz="Australia/Sydney"
            format="MMM D"
          >
            {datePosted}
          </Moment>
        </span>
      )}
    </Container>
  )
}

export default JobMeta
