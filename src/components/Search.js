import React from 'react'
import styled from 'styled-components/macro'
import { SearchBox } from 'react-instantsearch-dom'
import { FiSearch as IconSearch, FiX as IconReset } from "react-icons/fi";
import media from '../styles/style-media'
import Places from './Places'
import { SearchConsumer } from '../containers/SearchContext'

const SearchStyles = styled.div`
  display: flex;
  order: 2;
  flex: 0 0 100%;
  border-bottom: 1px solid var(--gray-1);

  ${media.desktop`
    order: inherit;
    flex: 0 1 auto;
    border-bottom: none;
  `};

  ${props => props.isHidden && 'visibility: hidden'};

  .ais-SearchBox {
    display: flex;
    flex: 0 1 50%;
    padding: 0;
  }

  .location-search {
    display: flex;
    position: relative;
    flex: 1 0 40%;
    border-left: 1px solid var(--gray-1);
    padding: 0;
  }

  .location-radius {
    flex: 0 0 20%;
    font-size: 0.9rem;
    display: flex;
    align-items: center;
  }

  input {
    padding: 1rem 1rem 1rem 2.75rem;
    width: 100%;
    height: 100%;
    border: 0;
    font-size: 0.9rem;

    ${media.desktop`
      padding: 1.5rem 2.5rem 1.5rem 3.75rem;
      font-size: 1rem;
    `};

    &::placeholder {
      color: var(--gray-3);
    }

    &:focus {
      outline: none;
    }

    &:required {
      &:valid {
        padding: 1rem 1.5rem 1rem 2.9rem;

        ${media.desktop`
          padding: 1.5rem 2.5rem 1.5rem 3.5rem;
        `};

        ~ .ais-SearchBox-reset {
          display: flex;
        }
      }
    }

    &::-webkit-search-cancel-button {
      display: none;
    }

    &.ap-input {
      padding: 1rem 1.6rem 1rem 2.8rem;

      ${media.desktop`
        padding: 1.5rem 2rem 1.5rem 3.75rem;
        font-size: 1rem;
      `};
    }
  }

  select {
    margin-right: 0.5rem;
    padding: 0 0 0 0.5rem;
    width: 100%;
    border: 0;
    border-left: 1px solid var(--gray-1);

    &:focus {
      outline: 0;
    }
  }

  button {
    width: 16px;
    height: 16px;
    top: 50%;
    transform: translateY(-50%);
    position: absolute;
    padding: 0;
    background: none;
    border: none;
    display: flex;
    justify-content: center;
    cursor: pointer;

    ${media.desktop`
      width: 19px;
      height: 19px;
    `};

    &[type='submit'] {
      left: 1rem;

      ${media.desktop`
        left: 1.5rem;
      `};
    }

    &[type='reset'] {
      right: 0.5rem;
      display: none;

      ${media.desktop`
        right: 1rem;
      `};
    }

    &.ap-icon-pin {
      display: none;
    }

    svg {
      width: 100%;
      height: 100%;
      stroke: var(--gray-3);

      &:hover {
        stroke: var(--gray-6) !important;
      }

      &.ap-input-icon {
        display: none;
      }
    }

    &.ap-icon-clear {
      right: 1rem;
      left: auto;

      svg {
        width: 10px;
        height: 10px;
        stroke-width: 0px;
      }
    }
  }
`

const Container = styled.div`
  margin: 0;
  padding: 0;
  width: 100%;
  display: flex;
  align-items: stretch;
`

const Search = ({ isHidden }) => (
  <SearchConsumer>
    {props => (
      <SearchStyles isHidden={isHidden}>
        <Container>
          <SearchBox
            submit={<IconSearch />}
            reset={<IconReset />}
            translations={{
              placeholder: 'Search all jobs',
            }}
          />
          <Places {...props} />
        </Container>
      </SearchStyles>
    )}
  </SearchConsumer>
)

export default Search