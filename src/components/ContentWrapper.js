import React from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components/macro'
import Sidebar from './Sidebar';
import media from '../styles/style-media'

const Container = styled.div`
  margin: 0;
  display: flex;
  height: ${props => props.isHome 
    ? 'calc(100% - var(--header-height-phone-withsearch))' 
    : 'calc(100% - var(--header-height-phone))' };

  ${media.desktop`
    height: calc(100% - var(--header-height));
  `}
`

const Content = styled.div`
  height: 100%;
  width: 100%;

  > div {
    display: flex;
    justify-content: center;
    overflow-x: hidden;
    overflow-y: auto;
    height: 100%;
    -webkit-overflow-scrolling: touch;
  }
`

const ContentWrapper = ({ children, location }) => (
  <Container isHome={ location.pathname === '/' }>
    <Sidebar />
    <Content id="content">
      {children}
    </Content>
  </Container>
)

export default withRouter(ContentWrapper)