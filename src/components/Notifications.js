import React from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive';
import styled, { css } from 'styled-components/macro'
import { FiBell as BellIcon } from 'react-icons/fi'
import Dropdown from './ui/Dropdown';
import media from '../styles/style-media'

const NotificationsIconContainer = styled.div`
  display: flex;
  position: relative;
  padding: 1rem; 

  ${media.desktop`
    padding: 1.5rem 2rem; 
  `};

  .badge-unread {
    ${props => props.unreadNotifications ? css`
      display: inline-flex;
      position: absolute;
      top: 16px;
      right: 19px;
      width: 7px;
      height: 7px;
      border-radius: 50%;
      background-color: var(--color-error);

      ${media.desktop`
        top: 24px;
        right: 35px;
      `};
    ` : `display: none`};
  }

  svg {
    margin: 0;
    width: 18px; 
    height: 18px; 
    stroke: ${props => props.unreadNotifications ? '#434343' : '#bfc9d1'};

    ${media.desktop`
      width: 22px; 
      height: 22px; 
    `};
  }
`

const NotificationsIcon = ({ unreadNotifications, toggleunreadNotifications }) => (
  <NotificationsIconContainer unreadNotifications={unreadNotifications} onClick={toggleunreadNotifications}>
    <BellIcon style={{  }} />
    <span className='badge-unread'></span>
  </NotificationsIconContainer>
)

const Notifications = ({ isAnonymous, isAuth, notifications, unreadNotifications, toggleunreadNotifications, onOpenModal }) => (
  <MediaQuery minWidth={768}>
    {(matches) => (
      <Dropdown 
        componentLabel={<NotificationsIcon unreadNotifications={unreadNotifications} toggleunreadNotifications={toggleunreadNotifications} />} 
        align="right"
        showArrow={false}
        buttonStyles={{ padding: 0 }}
        buttonClass='nav-link'
        style={{ margin: 0 }}
      >
        {isAnonymous && (
          <div>
            <p className='text-center'>You're currently in guest mode. When you're ready, we recommend that you create an account to ensure you don't lose any of your saved jobs or preferences.</p>
            <Link
              className='btn btn--primary margin-top'
              style={{ padding: '1rem' }}
              to={{
                pathname: '/signup',
                state: { modal: !!matches }
              }}
              onClick={() => matches && onOpenModal()}
            >
              Create an account
            </Link>
          </div>
        )}
    
        {/* TODO: Build Notifications Functionality */}
        {/* {!isAnonymous && notifications && (
            <>
              <div>
                <p>Display notification here</p>
              </div>
              <hr className='divider' />
              <div>
                <p>Display notification here</p>
              </div>
            </>
          )} */}
    
        {!isAnonymous && !notifications && (
          <div>
            <p className='text-center'>You have no new notifications</p>
          </div>
        )}
      </Dropdown>
    )}
  </MediaQuery>
)

export default Notifications