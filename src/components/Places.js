import React, { Component } from 'react'
import places from 'places.js'
import { createConnector } from 'react-instantsearch-dom'
import { FiMapPin as IconMapMarker } from "react-icons/fi";
import SelectComponent from './ui/Form/Select'

const connect = createConnector({
  displayName: 'LocationSearch',

  getProvidedProps() {
    return {}
  },

  refine(props, searchState, nextValue) {
    return {
      ...searchState,
      aroundLatLng: nextValue.latlng,
      aroundRadius: nextValue.radius,
      boundingBox: {},
      page: 1,
    }
  },

  getSearchParameters(searchParameters, props, searchState) {
    const currentLatLng = searchState.aroundLatLng
      ? `${searchState.aroundLatLng.lat}, ${searchState.aroundLatLng.lng}`
      : ''

    const currentRadius = searchState.aroundRadius || ''

    return searchParameters
      .setQueryParameter('insideBoundingBox')
      .setQueryParameter('aroundLatLng', currentLatLng)
      .setQueryParameter('aroundRadius', currentRadius)
  },
})

class Places extends Component {
  createRef = c => (this.element = c)

  componentDidMount() {
    const { refine, dispatch } = this.props

    this.autocomplete = this.initPlaces()

    this.autocomplete.on('change', event => {
      const location = event.suggestion.value || ''
      const latlng = event.suggestion.latlng || null
      const radius = 50000

      const newLocation = {
        location,
        latlng,
        radius,
      }

      // Update SearchContext
      dispatch({
        type: 'UPDATE_SEARCH_CONTEXT',
        newContext: newLocation,
      })

      refine(newLocation)
    })

    this.autocomplete.on('clear', () => {
      const clearLocation = { location: '', latlng: null, radius: 50000 }

      refine(clearLocation)

      // Clear Location SearchContext
      dispatch({
        type: 'CLEAR_SEARCH_CONTEXT',
      })
    })
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      const { refine, location } = this.props

      if (location) {
        // Set input value and refine search
        this.autocomplete.setVal(location)
        refine(this.props)
      }
    }
  }

  initPlaces = () =>
    places({
      container: this.element,
      type: 'city',
    })

  handleRadiusChange = e => {
    const { refine, dispatch } = this.props
    const radius = e.target.value

    // Update Radius in SearchContext
    dispatch({
      type: 'UPDATE_LOCATION_RADIUS',
      newRadius: radius,
    })

    refine(this.props)
  }

  render() {
    const { radius, location } = this.props

    return (
      <>
        <div className="location-search">
          <input
            ref={this.createRef}
            type="search"
            id="address-input"
            placeholder="Anywhere"
            defaultValue={location}
          />
          <button type="submit" aria-label="focus" className="icon-pin">
            <IconMapMarker className='stroke' />
          </button>
        </div>
        {radius && (
          <div className="location-radius">
            <SelectComponent
              id="location-radius"
              name="location-radius"
              label="Location Radius"
              hideLabel
              value={radius}
              onChange={this.handleRadiusChange}
            >
              <option value="10000">within 10km</option>
              <option value="50000">within 50km</option>
              <option value="100000">within 100km</option>
            </SelectComponent>
          </div>
        )}
      </>
    )
  }
}

export default connect(Places)
