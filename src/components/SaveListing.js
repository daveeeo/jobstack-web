import React from 'react'
import styled from 'styled-components/macro'
import { Layers } from './ui/Icons'
import withAuthContext from '../containers/withAuthContext';

const SaveButton = styled.button`
  background: none;
  border: none;
  padding: 0.25rem;
  text-align: center;
  cursor: pointer;
  display: flex;
  align-items: center;

  svg {
    stroke: var(--gray-4);
    transition: all ease-in-out 0.18s;
    width: 22px;
    height: 20px;
  }

  &:hover {
    svg {
      stroke: var(--gray-6);
    }
  }

  &.saved {
    svg {
      stroke: var(--color-primary);
    }
  }

  &:focus {
    outline: none;
  }

  .label {
    margin-left: 0.75rem;
  }
`

const SaveListing = ({ context, job, showLabel, style }) => { 
  const isJobSaved = context.savedJobs.find(el => el.objectID === job.objectID)
  const label = !isJobSaved ? 'Save' : 'Remove'

  const handleClick = (e) => {
    e.preventDefault()

    if (showLabel) {
      const node = e.target.closest('.save-listing').querySelector('.label')
      node.textContent = isJobSaved ? 'Removing...' : 'Saving...'
    }
    
    context.toggleSaveListing(job)
  }

  return (
    <>
      <SaveButton
        className={`save-listing ${isJobSaved && 'saved'}`}
        onClick={handleClick}
        style={style}
      >
        <Layers />
        {showLabel && <span className="label">{label}</span>}
      </SaveButton>
    </>
  )
}

export default withAuthContext(SaveListing)
