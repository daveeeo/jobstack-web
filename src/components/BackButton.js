import React from 'react'
import styled from 'styled-components/macro'
import { FiArrowLeft as IconBack } from "react-icons/fi";
import Button from './ui/Elements/Button';

const BackButtonContainer = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  
  button {
    padding: 0.5rem 0.5rem 0.5rem 0;
  }

  svg {
    width: 20px;
    height: 20px;
    margin-right: 0.5rem;
  }
`

const BackButton = ({ onClick, className, style, label = 'Back' }) => (
  <BackButtonContainer className={`btn-back ${className}`} style={style}>
    <Button className='btn-empty' onClick={onClick}>
      <IconBack />
      {label}
    </Button>
  </BackButtonContainer>
)

export default BackButton