import React from 'react'
import styled from 'styled-components/macro'
import { FaUserCircle as DisplayPicturePlaceholder } from "react-icons/fa";
import Loading from './Loading';

const DisplayPictureContainer = styled.span`
  width: 40px;
  height: 40px;
  position: relative;
  overflow: hidden;
  border-radius: 50%;
  background-color: white;
  border: 1px solid var(--gray-1);
  margin: 0 0.75rem 0 0;
  flex: 0 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    transform: translate(-50%, -50%);
  }

  svg {
    width: 100%;
    height: 100%;
    fill: var(--gray-2);
  }
`

const DisplayPicture = ({ style, displayPicture, asElement, isUploading }) => (
  <DisplayPictureContainer style={style} as={asElement}>
    {isUploading && <Loading
      as='span'
      spinnerColor='var(--color-primary)'
      style={{ 
        position: 'absolute',
        zIndex: 1,
        background: 'rgba(255,255,255,0.9)' }} />}
    {displayPicture ? (
      <img
        src={displayPicture}
        alt=''
      />
    ) : (
      <DisplayPicturePlaceholder />
    )}
  </DisplayPictureContainer>
)

export default DisplayPicture
