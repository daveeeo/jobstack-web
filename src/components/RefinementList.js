/* eslint-disable no-underscore-dangle */
import React from 'react'
import {
  connectCurrentRefinements,
  connectRefinementList,
  Highlight,
} from 'react-instantsearch-dom'
import styled from 'styled-components/macro'

const RefinementListWrapper = styled.div`
  display: flex;
  flex-flow: column wrap;
  width: 100%;

  .filter-search {
    padding: 0.5rem 0.75rem;
    border: 1px solid var(--gray-1);
    margin-bottom: 1rem;
    font-size: 0.95rem;
  }

  ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    li {
      .filter-label {
        display: flex;
        align-items: center;
        cursor: pointer;
        padding: 0 0 0.5rem;

        .filter-checkbox {
          border: 0;
          clip-path: inset(1px 1px 1px 1px);
          height: 1px;
          margin: -1px;
          overflow: hidden;
          padding: 0;
          position: absolute;
          width: 1px;
        }
        .filter-text-label {
          flex-grow: 1;
          padding-left: 2rem;
          position: relative;

          &::before,
          &::after {
            content: '';
            position: absolute;
            display: inline-block;
          }

          &::before{
            height: 21px;
            width: 21px;
            border: 1px solid var(--gray-1);
            left: 0px;
            top: 0px;
          }

          &::after {
            height: 5px;
            width: 9px;
            border-left: 2px solid white;
            border-bottom: 2px solid white;
            transform: rotate(-45deg);
            left: 6px;
            top: 7px;
          }
        }
        .filter-count {
          background-color: var(--color-body);
          border-radius: 4px;
          padding: 0.25rem;
          font-size: 0.8rem;
          text-align: center;
          min-width: 34px;
          color: var(--gray-6);
        }

        .filter-checkbox + .filter-text-label::after {
            content: none;
        }

        .filter-checkbox:checked + .filter-text-label {
          color: var(--gray-10);
        }

        .filter-checkbox:checked + .filter-text-label::before {
          background-color: var(--color-primary);
          border-color: var(--color-primary);
        }

        .filter-checkbox:checked + .filter-text-label::after {
            content: '';
        }

        &:hover {
          .filter-checkbox + .filter-text-label {
            color: var(--gray-10);
          }
        }
      }
    }
  }
`

const Label = styled.span`
  background-color: white;
  border: 1px solid var(--gray-1);
  padding: 0.75rem 2rem 0.75rem 0.75rem;
  margin: 0 0.5rem 0.5rem 0;
  text-align: center;
  border-radius: 6px;
  display: inline-block;
  font-size: var(--text-sm);
  position: relative;
`

const CloseButton = styled.span`
  position: absolute;
  top: 5px;
  right: 7px;
  cursor: pointer;

  &:hover {
    color: var(--color-error);
  }
`

const CurrentFilterListStyles = styled.div`
  display: flex;
  flex-flow: row wrap;
`

const CurrentRefinementList = connectCurrentRefinements(
  ({ refine, dispatch, items }) => (
    <CurrentFilterListStyles>
      {items.map(el =>
        el.items.map(item => (
          <Label key={item.label}>
            <span>{item.label}</span>
            <CloseButton
              onClick={e => {
                refine(item.value)
                dispatch({
                  type: 'REMOVE_FILTER',
                  removeFilter: item.label,
                })
              }}
            >
              ✕
            </CloseButton>
          </Label>
        )),
      )}
    </CurrentFilterListStyles>
  ),
)

const RefinementList = connectRefinementList(
  ({ dispatch, items, refine, searchForItems, defaultRefinement }) => {

    const handleRefinement = (item) => {
      refine(item.value)
      if (!defaultRefinement.includes(item.label)) {
        dispatch({
          type: 'ADD_FILTER',
          newFilter: item.label,
        })
      } else {
        dispatch({
          type: 'REMOVE_FILTER',
          removeFilter: item.label,
        })
      }
    }

    const handleEnterKey = (e) => {
      const keyCode = e.keyCode || e.which;

      if (keyCode === 13) {
        const node = document.querySelector('#refinement-list').firstElementChild
        const labelText = node.querySelector('.filter-text-label').textContent

        const item = items.find((el) => el.label === labelText)
        refine(item.value)
      }

    }

    const values = items.map(item => {
      const label = item._highlightResult ? (
        <Highlight attribute="label" hit={item} tagName="strong" />
      ) : (
        item.label
      )

      return (
        <li key={item.value} className={item.isRefined ? 'filter-selected' : undefined}>
          <label
            className="filter-label" htmlFor='filter-item' onClick={() => handleRefinement(item)}>
            <input className="filter-checkbox" name='filter-item' type="checkbox" checked={item.isRefined && 'checked'} readOnly />
            <span className="filter-text-label">{label}</span> 
            <span className="filter-count">{item.count}</span>
          </label>
        </li>
      )
    })

    return (
      <RefinementListWrapper>
        <h3>Filters</h3>
        <input
          className='filter-search'
          type="search"
          placeholder="Type a skill"
          onInput={e => searchForItems(e.target.value)}
          onKeyPress={handleEnterKey}
        />
        <ul id='refinement-list'>{values}</ul>
        {/* <CurrentRefinementList dispatch={dispatch} /> */}
      </RefinementListWrapper>
    )
  },
)

export { CurrentRefinementList, RefinementList }
