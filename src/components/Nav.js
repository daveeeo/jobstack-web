import React from 'react'
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive';
import styled from 'styled-components/macro'
import { FiSettings as IconSettings } from 'react-icons/fi'
import { AuthConsumer } from '../containers/AuthContext'
import Dropdown from './ui/Dropdown'
import withModalContext from '../containers/withModalContext'
import Notifications from './Notifications';
import media from '../styles/style-media'

const NavStyles = styled.div`
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: flex-end;
  align-items: stretch;
  flex: 1;
  
  ${media.phone`
    border-bottom: 1px solid var(--gray-1);
  `};

  ${media.desktop`
    border-bottom: none;
  `};

  a,
  button {
    text-transform: uppercase;
    font-weight: 600;
    padding: 1rem; 
    color: inherit;

    ${media.desktop`
      padding: 1.5rem 2rem; 
    `};
  }

  .btn--primary {
    color: white;
  }

  .nav-link {
    display: flex;
    align-items: center;
    border-left: 1px solid var(--gray-1);
  }

  .saved {
    svg {
      width: 21px;
      height: 21px;
      stroke-width: 1.75;
    }
  }
`

const IconSettingsStyled = styled(IconSettings)`
  width: 18px;
  height: 18px;
  stroke: #bfc9d1;

  ${media.desktop`
    width: 22px; 
    height: 22px; 
  `};
`

const Nav = ({ context }) => (
  <MediaQuery minWidth={1170}>
    {(matches) => (
      <AuthConsumer>
        {({ isAuth, signOut, isAnonymous, notifications, unreadNotifications, toggleunreadNotifications }) => (
          <NavStyles>
            <>
              {isAuth ? (
                <>
                  <Notifications 
                    isAuth={isAuth} 
                    isAnonymous={isAnonymous} 
                    notifications={notifications} 
                    unreadNotifications={unreadNotifications}
                    toggleunreadNotifications={toggleunreadNotifications}
                    onOpenModal={context.onOpenModal}
                  />
                  <Dropdown 
                    componentLabel={<IconSettingsStyled />} 
                    align="right"
                    showArrow={false}
                    style={{ margin: 0 }}
                    buttonClass='nav-link'
                  >
                    {!isAnonymous && <Link to="/account" aria-label='Edit Account'>Edit Account</Link>}
                    <Link to="/saved" className='hidden-tablet' aria-label='Saved Jobs'>Saved Jobs</Link>
                    <Link to="/" onClick={() => signOut()} aria-label='Sign out of guest mode'>
                      {isAnonymous ? 'Sign out of guest mode' : 'Sign out'}
                    </Link>
                  </Dropdown>
                </>
              ) : (
                <>
                  <Link
                    className='nav-link'
                    to={{
                      pathname: '/login',
                      state: { modal: !!matches }
                    }}
                    onClick={() => matches && context.onOpenModal()}
                    aria-label='Login'
                  >
                    Login
                  </Link>

                  <Link
                    className='nav-link'
                    to={{
                      pathname: '/signup',
                      state: { modal: !!matches }
                    }}
                    onClick={() => matches && context.onOpenModal()}
                    aria-label='Sign up'
                  >
                    Sign up
                  </Link>
                </>
              )}
            </>
          </NavStyles>
        )}
      </AuthConsumer>
    )}
  </MediaQuery>
)

export default withModalContext(Nav)
