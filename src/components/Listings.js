import React from 'react'
import styled from 'styled-components/macro'
import {
  connectInfiniteHits,
  connectStats,
  // connectStateResults,
} from 'react-instantsearch-dom'
import Listing from './Listing'
import media from '../styles/style-media'

const LoadMore = styled.button`
  background-color: white;
  border: 1px solid var(--gray-1);
  padding: 1rem;
  width: 100%;
  margin: 0 0 1.5rem;
  cursor: pointer;
  transition: all ease-in-out 0.18s;

  &:focus {
    outline: none;
  }
  &:hover {
    color: var(--gray-10);
    box-shadow: 0 4px 10px rgba(47, 47, 87, 0.07),
      0 2.5px 5px rgba(0, 0, 0, 0.07);
  }
`

const ListingsHeader = styled.div`
  display: flex;
  margin-bottom: 15px;
  padding: 0 0.5rem;
  align-items: center;
  min-height: 32px;

  h1 {
    flex: 1;
    font-size: 1.2rem;
    margin: 0 1rem 0 0;
    
    ${media.tablet`
      font-size: 1.3rem;
    `}
  }

  span {
    font-size: 0.9rem;
  }
`

const Stats = ({ nbHits }) => (
  <span>{ nbHits } results</span>
)

const ConnectedStats = connectStats(Stats)



// TODO: Utilize connectStateResults for loading states

const Listings = ({ hits, refine, hasMore }) => (
  <>
    <ListingsHeader>
      <h1>Latest programming jobs</h1>
      <span><ConnectedStats /></span>
    </ListingsHeader>
    
    <div>
      {hits.map(hit => (
        <Listing key={hit.objectID} hit={hit} />
      ))}
    </div>

    {hasMore && (
      <LoadMore className="ignore-react-onclickoutside" onClick={refine}>
        Load more jobs
      </LoadMore>
    )}
  </>
)

export default connectInfiniteHits(Listings)
