import React, { Component } from 'react'
import styled, { css } from 'styled-components/macro'
import { FiXCircle as IconClose } from 'react-icons/fi'
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  RedditShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  RedditIcon
} from 'react-share';
import Button from './ui/Elements/Button';
import media from '../styles/style-media'
const onClickOutside = require('react-onclickoutside').default

const PopoverContainer = styled.div(props => css`
  .popover,
  .popover-trigger {
    position: fixed;
    bottom: 0;
    left: 0;
    -webkit-transition: -webkit-transform .2s;
    transition: -webkit-transform .2s;
    transition: transform .2s;
    transition: transform .2s,-webkit-transform .2s;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-backface-visibility: hidden;
    will-change: transform;
    backface-visibility: hidden;
  }

  .popover-trigger {
    z-index: 3;
    height: calc(var(--sidebar-width-tablet) - 1px);
    width: calc(var(--sidebar-width-tablet) - 1px);
    text-indent: 100%;
    color: transparent;
    white-space: nowrap;

    ${media.desktop`
      height: calc(var(--sidebar-width) - 1px);
      width: calc(var(--sidebar-width) - 1px);
    `}

    svg {
      width: 24px;
      height: 24px;
      stroke: #bfc9d1;
    }
  }

  .popover {
    z-index: 2;
    width: 90%;
    max-width: 440px;
    height: 360px;
    max-height: 90%;
    pointer-events: none;

    .popover-wrapper {
      position: absolute;
      bottom: 0;
      left: 0;
      z-index: 2;
      overflow: hidden;
      height: calc(var(--sidebar-width-tablet) - 1px);
      width: calc(var(--sidebar-width-tablet) - 1px);
      -webkit-transition: height .4s .1s,width .4s .1s,box-shadow .3s;
      transition: height .4s .1s,width .4s .1s,box-shadow .3s;
      -webkit-transition-timing-function: cubic-bezier(.67,.17,.32,.95);
      transition-timing-function: cubic-bezier(.67,.17,.32,.95);
      background: #fff;
      pointer-events: auto;

      ${media.desktop`
        height: calc(var(--sidebar-width) - 1px);
        width: calc(var(--sidebar-width) - 1px);
      `}

      ${props.isOpen && css`
        height: 100% !important;
        width: 100% !important;
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        box-shadow: var(--box-shadow-container);
      `};

      .popover-body {
        position: relative;
        z-index: 1;
        height: 100%;
        -webkit-overflow-scrolling: touch;
        transition: opacity .2s;
        opacity: 0;
        padding: 0;

        ${props.isOpen && css`
          opacity: 1;
          transition: opacity .2s .3s;
        `};

        .popover-content {
          padding: 2.25rem;

          h1 {
            font-size: 1rem;
            text-transform: uppercase;
            margin-bottom: 1rem;
          }

          p {
            font-size: 1rem;
            color: var(--color-text);
          }
        }

        footer {
          position: absolute;
          z-index: 2;
          bottom: 0;
          width: 100%;
          display: flex;
          align-items: center;
          justify-content: flex-end;
          padding: 2rem 2.5rem;

          span {
            text-transform: uppercase;
            font-size: 0.9rem;
            font-weight: bold;
            margin-right: 0.5rem;
          }

          a {
            padding: 1.55rem 0.5rem;
            height: auto;
          }

          .SocialMediaShareButton {
            margin-left: 0.5rem;
            cursor: pointer;
          }
        }
      }
    }
  }
`)

class Popover extends Component {
  state = {
    isOpen: false
  }

  handleClick = () => {
    const { isOpen } = this.state
    this.setState({ isOpen: !isOpen })
  }

  handleClickOutside = () => this.setState({ isOpen: false })

  render() {
    const { isOpen } = this.state
    const { trigger, content } = this.props
    return (
      <PopoverContainer isOpen={isOpen}>
        <Button 
          onClick={this.handleClick} 
          type='button' 
          className='btn btn-empty popover-trigger'
          role='button'
          ariaLabel='Close button'
        >
          {!isOpen ? trigger : <IconClose />}
        </Button>
        <div className='popover'>
          <div className='popover-wrapper'>
            <div className='popover-body'>
              <div className='popover-content'>
                {content}
              </div>
              <footer>
                <span>Share on</span>
                <TwitterShareButton 
                  url='https://jobstack.co'
                  title='jobstack.co - Programming jobs for Front-end, Full-stack, and Back-end Developers'
                  via='jobstackhq'
                >
                  <TwitterIcon size={24} round />
                </TwitterShareButton>

                <RedditShareButton 
                  url='https://jobstack.co'
                  title='jobstack.co - Programming jobs for Front-end, Full-stack, and Back-end Developers'
                >
                  <RedditIcon size={24} iconBgStyle={{ fill: '#ff4500' }} round />
                </RedditShareButton>

                <LinkedinShareButton 
                  url='https://jobstack.co'
                  title='jobstack.co - Programming jobs for Front-end, Full-stack, and Back-end Developers'
                  description='JobStack finds the latest programming jobs from around the world to provide you with a fast and easy-to-use job searching experience.'
                >
                  <LinkedinIcon size={24} round />
                </LinkedinShareButton>

                <FacebookShareButton 
                  url='https://jobstack.co'
                  quote='jobstack.co - Programming jobs for Front-end, Full-stack, and Back-end Developers'
                >
                  <FacebookIcon size={24} round />
                </FacebookShareButton>
              </footer>
            </div>
          </div>
        </div>
      </PopoverContainer>
    )
  }
}

export default onClickOutside(Popover)