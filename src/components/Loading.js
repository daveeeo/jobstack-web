import React from 'react'
import styled from 'styled-components/macro'
import { LoadingSpinner } from './ui/Icons';

const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  position: relative;

  svg {
    rect {
      fill: ${props => props.spinnerColor && props.spinnerColor};
    }
  }
`

const Loading = ({ style, spinnerStyle, spinnerColor, asElement }) => (
  <LoadingContainer spinnerColor={spinnerColor} style={style} as={asElement}>
    <LoadingSpinner style={spinnerStyle} />
  </LoadingContainer>
)

export default Loading