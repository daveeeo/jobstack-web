import React from 'react'
import styled from 'styled-components/macro'
import { Business } from './ui/Icons'

const Logo = styled.span`
  width: 60px;
  height: 60px;
  position: relative;
  overflow: hidden;
  border-radius: 50%;
  background-color: white;
  border: 1px solid var(--gray-1);
  margin: 0 0.75rem 0 0;
  flex: 0 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 100%;
    width: auto;
    transform: translate(-50%, -50%);
  }
`

const CompanyLogo = ({ style, logo, companyName, className }) => (
  <Logo style={style} className={className}>
    {logo ? (
      <img
        src={logo}
        alt={`${companyName ? `${companyName} | jobstack` : 'jobstack'}`}
      />
    ) : (
      <Business
        style={{
          width: '50%',
          height: '50%',
          fill: 'var(--color-primary)',
        }}
      />
    )}
  </Logo>
)

export default CompanyLogo
