import React from 'react'
import { Link } from "react-router-dom";
import styled from 'styled-components/macro'

const FooterContainer = styled.footer`
  display: flex;
  padding: 0 1.5rem;
  font-size: 0.9rem;
  align-items: center;
  justify-content: center;
  flex-flow: row wrap;
  line-height: 1.5;

  .middot {
    margin: 0 0.75rem;
  }

  a {
    color: inherit;

    &:hover {
      text-decoration: underline;
    }
  }
`

const Footer = () => (
  <FooterContainer>
    <span>JobStack © {(new Date().getFullYear())}</span>
    <span className='middot'></span>
    <Link to='/contact'>Contact</Link>
    <span className='middot'></span>
    <a href='https://jobstack.typeform.com/to/ajQeY2' target='_blank' rel='noopener noreferrer'>Post a job</a>
  </FooterContainer>
)

export default Footer