import React from 'react'
import { Route, Link } from 'react-router-dom'
import styled from 'styled-components/macro'
import Moment from 'react-moment'
import media from '../styles/style-media'
import { truncate } from '../styles/style-utils'
import 'moment-timezone'

import Panel from './Panel'
import CompanyLogo from './CompanyLogo'
import Loading from './Loading';

const ListContainer = styled.ul`
  margin: 0;
  padding: 0;
`

const LinkStyled = styled(Link)`
  display: flex;
  padding: 1.5rem;
  border-bottom: 1px solid var(--gray-1);
`

const JobDetails = styled.div`
  flex: 1;

  h4 {
    font-size: var(--text-base-size);
    margin-bottom: var(--space-xxxs);

    ${media.desktop`
      ${truncate('210px')};
    `};

    ${media.giant`
      ${truncate('230px')};
    `};
  }

  span {
    color: var(--gray-6);
    font-size: var(--text-sm);
  }
`

const CustomLink = ({ to, children }) => (
  <Route
    path={to.pathname}
    children={({ match }) => (
      <li>
        <LinkStyled
          to={to.pathname}
          style={{
            backgroundColor: match && 'white',
            boxShadow:
              match &&
              '0 -10px 20px rgba(0,0,0,0.02), 0 10px 20px rgba(0,0,0,0.02)',
          }}
        >
          {children}
        </LinkStyled>
      </li>
    )}
  />
)

const Subnav = ({ title, list, loading, location, match }) =>
  loading ? (
    <Loading spinnerColor='var(--color-primary)' spinnerStyle={{ width: '60px', height: '60px' }} />
  ) : (
    <Panel type="sidenav" scrollbar="hidden">
      {title && <h3>{title}</h3>}
      <ListContainer>
        {list &&
          list.map(item => (
            <CustomLink
              key={item.objectID}
              to={{
                pathname: `${match.url}/${item.slug}/${item.objectID}`,
                search: location.search,
              }}
            >
              <CompanyLogo
                style={{ width: '40px', height: '40px' }}
                logo={item.companyLogo}
                companyName={item.companyName}
              />
              <JobDetails>
                {item.title && <h4>{item.title}</h4>}
                {item.datePosted && (
                  <span>
                    Posted&nbsp;
                    <Moment
                      unix
                      fromNowDuring={172800000}
                      tz="Australia/Sydney"
                      format="MMM D"
                    >
                      {item.datePosted}
                    </Moment>
                  </span>
                )}
              </JobDetails>
            </CustomLink>
          ))}
      </ListContainer>
    </Panel>
  )

export default Subnav
