import React from 'react'
import styled, { css } from 'styled-components/macro'
import media from '../styles/style-media'

const PanelStyled = styled.div`
  display: relative;
  ${media.tablet`
    display: grid;
    height: calc(100vh - var(--header-height));
    background-color: ${props =>
    props.type === 'sidenav' && 'var(--color-body)'};
    background-color: ${props => props.type === 'content' && 'white'};
  `};

  .panel-body {
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    box-shadow: ${props =>
    props.type === 'content' && '-5px 0 15px rgba(0,0,0,0.01)'};
    border-left: ${props =>
    props.type === 'content' && '1px solid var(--gray-1)'};

    ${props =>
    props.type === 'sidenav' &&
      props.scrollbar === 'hidden' &&
      css`
        -ms-overflow-style: none;
        overflow: -moz-scrollbars-none;
        &::-webkit-scrollbar {
          width: 0 !important;
        }
      `};
  }
`

const Panel = ({ children, type, scrollbar }) => (
  <PanelStyled type={type} scrollbar={scrollbar}>
    <div className="panel-body">{children}</div>
  </PanelStyled>
)

export default Panel
