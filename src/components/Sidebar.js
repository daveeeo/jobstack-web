import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components/macro'
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap_white.css';
import { FiHelpCircle as IconHelp } from 'react-icons/fi'
import { Layers, Listings } from './ui/Icons'
import Popover from './Popover';
import About from './About';
import media from '../styles/style-media'

const SidebarStyles = styled.aside`
  height: 100%;
  width: var(--sidebar-width-tablet);
  background-color: white;
  border-right: 1px solid var(--gray-1);
  flex-shrink: 0;
  align-items: stretch;
  flex-direction: column;
  padding: 1.5rem 0 0;
  position: relative;
  display: none;

  ${media.tablet`
    display: flex;
  `};

  ${media.desktop`
    width: var(--sidebar-width);
    padding: 2rem 0 0;
  `};


  a, button {
    height: 4rem;
    display: flex;
    justify-content: center;
    align-items: center;

    ${media.desktop`
      height: 5rem;
    `};

    svg {
      width: 24px;
      height: 24px;
      &.fill {
        fill: #bfc9d1;
      }

      &.stroke {
        stroke: #bfc9d1;
      }
    }

    &.active {
      svg {
        &.fill {
          fill: var(--color-primary);
        }

        &.stroke {
          stroke: var(--color-primary);
        }
      }
    }

    &.home {
      svg {
        width: 22px;
        height: 22px;

        ${media.desktop`
          width: 26px;
          height: 26px;
        `};
      }
    }

    &.saved-jobs {
      svg {
        width: 26px;
        height: 22px;

        ${media.desktop`
          width: 32px;
          height: 28px;
        `};
      }
    }
  }
`

const TopNav = styled.div`
  flex-grow: 1;
`

const BottomNav = styled.div``

const Sidebar = () => (
  <SidebarStyles id='sidebar-main'>
    <TopNav>
      <Tooltip
        trigger={['hover']} 
        mouseEnterDelay={0.4} 
        placement="right" 
        overlay={<span>Home</span>}
        transitionName='tooltip-transition'
        align={{
          offset: [-14, 0],
        }}>
        <NavLink exact className='nav-link home' activeClassName="active" to="/" aria-label='Home'>
          <Listings className="fill" />
        </NavLink>
      </Tooltip>

      <Tooltip
        trigger={['hover']} 
        mouseEnterDelay={0.4} 
        placement="right" 
        overlay={<span>Saved Jobs</span>}
        transitionName='tooltip-transition'
        align={{
          offset: [-14, 0],
        }}>
        <NavLink className='nav-link saved-jobs' activeClassName="active" to="/saved" aria-label='Saved Jobs'>
          <Layers className="stroke" />
        </NavLink>
      </Tooltip>

    </TopNav>
    <BottomNav>
      <Popover 
        trigger={<Tooltip
          trigger={['hover']} 
          mouseEnterDelay={0.4} 
          placement="right" 
          overlay={<span>About jobstack</span>}
          transitionName='tooltip-transition'
          align={{
            offset: [18, 0],
          }}>
          <IconHelp className="stroke" />
        </Tooltip>}
        content={<About />}
      />
      
    </BottomNav>
  </SidebarStyles>
)

export default Sidebar
