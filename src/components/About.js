import React from 'react'

const About = () => (
  <>
    <h1 className='title'>About JobStack</h1>
    <p>JobStack finds the latest programming jobs from around the world to provide you with a fast and simple job searching experience.</p>
    <p>You can search, filter, and save jobs without having to sign up. When you're ready, we do recommend that you create an account to ensure you don't lose any of your saved jobs or search preferences!</p>
  </>
)

export default About