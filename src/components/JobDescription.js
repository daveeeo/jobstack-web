import React from 'react'
import styled, { css } from 'styled-components/macro'
import Helmet from 'react-helmet-async'
import sanitizeHtml from 'sanitize-html'
import MediaQuery from 'react-responsive';
import JobMeta from './JobMeta'
import SaveListing from './SaveListing'
import Button from './ui/Elements/Button'
import Loading from './Loading';
import media from '../styles/style-media'
import BackButton from './BackButton';
import withModalContext from '../containers/withModalContext';

const Container = styled.section`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;

  .job-wrapper {
    ${props => props.isModal && css`
      background: white;
      box-shadow: var(--box-shadow-container);
      width: 100%;
      margin: auto;
      pointer-events: auto;

      ${media.tablet`
        width: 640px;
      `}

      ${media.desktop`
        width: 840px;
      `}
    `}
  }

  .job-header {
    display: flex;
    padding: 1rem;
    border-bottom: 1px solid var(--gray-1);
    align-items: center;
    flex-shrink: 0;

    /* Move to isModal prop if this breaks on other pages */
    position: sticky;
    top: 0;
    left: 0;
    right: 0;
    background: white;
    z-index: 1;

    ${media.tablet`
      padding: 1rem 2rem;
    `}

    .apply {
      display: flex;
      align-items: center;
      flex: 0;

      a {
        margin-left: 1rem;
      }
    }
  }

  .job-details {
    background-color: white;
    padding: 2rem 1rem;
    flex-grow: 1;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    font-size: 1.05rem;
    line-height: 1.75;

    ${media.tablet`
      padding: 2rem;
    `}

    h1 {
      font-family: var(--font-logo);
      font-size: 1.75rem;
      margin: ${props => props.isModal ? `0.5rem 0 1.5rem` : `0 0 1.5rem`};

      ${media.tablet`
        font-size: var(--text-xl);
      `}

      ${media.desktop`
        font-size: var(--text-xxl);
      `}
    }

    p,
    ul {
      font-size: 1.05rem;
      line-height: 1.75;
    }

    p > strong {
      margin-top: 1rem;
      display: inline-block;
    }

    .subtitle {
      text-transform: uppercase;
      font-size: 1rem;
      margin-bottom: 1rem;
    }
  }
`

const JobMetaStyled = styled(JobMeta)`
  display: none;

  ${media.tablet`
    display: inherit;
  `};
`

const Label = styled.span`
  background-color: white;
  border: 1px solid var(--gray-1);
  padding: 0.2rem 0.75rem;
  margin: 0 0.5rem 0.5rem 0;
  text-align: center;
  border-radius: 6px;
  display: inline-block;
  font-size: var(--text-sm);
`

const JobDescription = ({ loading, job, isModal, isSavedJobs, context }) => {
  const { onCloseModal } = context

  const metaDescription = job && `${sanitizeHtml(job.description, {
    allowedTags: [],
    allowedAttributes: []
  }).substring(0, 140)}...`

  return (
    <Container isModal={isModal}>
      {loading ? (
        <Loading spinnerColor='var(--color-primary)' spinnerStyle={{ width: '60px', height: '60px' }} />
      ) : (
        <div className='job-wrapper'>
          <Helmet>
            <title>{job.title} - jobstack.co</title>
            <meta name="description" content={metaDescription} />
          </Helmet>
          <header className="job-header">
            <MediaQuery minWidth={1024}>
              {(matches) => {
                if (matches) {
                  if (!isModal) {
                    return (
                      <JobMetaStyled
                        logo={job.companyLogo}
                        companyName={job.companyName}
                        datePosted={job.datePosted}
                        isModal={isModal}
                      />
                    )
                  }
                }
                return <BackButton onClick={onCloseModal} />
              }}
            </MediaQuery>
            <div className="apply">
              <SaveListing job={job} />
              <Button as="a" href={job.jobUrl} target="_blank" style={{ marginLeft: '2rem' }}>
                View job
              </Button>
            </div>
          </header>
          <div className="job-details">
            {isModal && (
              <JobMeta
                logo={job.companyLogo}
                companyName={job.companyName}
                datePosted={job.datePosted}
                style={{ marginBottom: '1rem' }}
              />
            )}
            <MediaQuery maxWidth={1023}>
              <JobMeta
                logo={job.companyLogo}
                companyName={job.companyName}
                datePosted={job.datePosted}
                style={{ marginBottom: '1rem' }}
              />
            </MediaQuery>
  
            <h1 
              dangerouslySetInnerHTML={{
                __html: job.title,
              }} />
            <div dangerouslySetInnerHTML={{ __html: job.description }} />
            <hr className='divider large' />
            <div>
              <h4 className='subtitle'>Location</h4>
              {job.remoteFriendly ? (
                <span className="location">Remote</span>
              ) : (
                <span className="location">{`${job.jobLocation[0].city}, ${job.jobLocation[0].country}`}</span>
              )}
            </div>
            <hr className='divider large' />
            <div>
              <h4 className='subtitle'>Skills</h4>
              {job.categories &&
                job.categories
                  .map(category => <Label key={category}>{category}</Label>)}
            </div>
          </div>
        </div>
      )}
    </Container>
  )
}

export default withModalContext(JobDescription)
