import { css } from 'styled-components/macro'
import media from './style-media'

const GlobalStylesModals = css`
  .overlay {
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    z-index: 3;
    background-color: rgba(255, 255, 255, 0.75);
    overflow-x: hidden;
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;

    ${media.desktop`
      display: flex;
      align-items: center;
      justify-content: center;
    `}
  }

  .modal {
    display: flex;
    width: auto;
    max-width: 500px;
    margin: 1.75rem auto;

    ${media.tablet`
      position: relative;
      margin: 0;
      max-width: 800px;
    `}
  }

  .modal-job {
    min-height: 250px;
    /* margin-bottom: 20px; */
    position: fixed;
    top: var(--header-height-phone);
    left: 0;
    right: 0;
    margin: auto;
    width: 100%;
    height: 100%;
    /* transform: translate3d(0,0,0); */
    /* box-shadow: var(--box-shadow-container);
    background-color: white; */

    > section,
    > div {
      height: calc(100% - var(--header-height-phone));
      width: 100%;
      overflow-y: scroll;
      -webkit-overflow-scrolling: touch;
    }

    ${media.phone`
      /* width: 100%;
      margin-left: -50%;
      top: 0 !important; */
    `};

    ${media.tablet`
      /* width: 640px; */
      /* margin-left: -320px; */
      /* top: var(--header-height); */
      top: var(--header-height);
      pointer-events: none;

      > section,
      > div {
        height: calc(100% - var(--header-height));
      }
    `};

    ${media.desktop`
      /* width: 840px;
      margin-left: -420px; */
    `};
  }

  .modal:focus,
  .modal-job:focus {
    outline: none;
  }

  /* Hide overflow when modal is active */
  /* .modal-body,
  .modal-job-body {
    #content > div {
      overflow: hidden;
    }
  } */

  /* Adjust z-index for Header and Aside elements when modal-job is active */

  .modal-job-body {
    #header-main,
    #sidebar-main {
      z-index: 30;
    }
  }
`

export default GlobalStylesModals
