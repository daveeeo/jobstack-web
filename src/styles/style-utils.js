// these sizes are arbitrary and you can set them to whatever you wish
import { createGlobalStyle } from 'styled-components'
import GlobalStylesTypography from './style-typography'
import GlobalStylesColours from './style-colours'
import GlobalStylesSpacing from './style-spacing'
import GlobalStylesButtons from './style-buttons'
import GlobalStylesModals from './style-modals'
import media from './style-media'

/** --------------------
 *
 *    Global Style
 *
 * --------------------- */

export const GlobalStyle = createGlobalStyle`
  *, *:before, *:after {
    box-sizing: inherit;
  }

  :root {
    --box-shadow-container: 0 5px 15px rgba(0,0,0,.05), 0 -2px 60px rgba(0,0,0,.05);
    --box-shadow-container-light: 0 5px 15px rgba(0,0,0,.03), 0 -2px 60px rgba(0,0,0,.03);
    --radius: 4px;
  }

  html {
    box-sizing: border-box;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }
  body {
    background-color: var(--color-body);
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -moz-font-feature-settings: "liga" on;
  }
  body, html {
    padding: 0;
    margin: 0;
    height: 100%;
    overflow: hidden;
  }
  a {
    text-decoration: none;
  }

  #root,
  .ais-InstantSearch__root {
    height: 100%;   /* needed for fixed positioning with flex */
  }

  ${GlobalStylesTypography};
  ${GlobalStylesColours};
  ${GlobalStylesSpacing};
  ${GlobalStylesButtons};
  ${GlobalStylesModals};


  /** --------------------
  *    Responsive utility classes
  * --------------------- */

  .hidden-phone {
    display: none !important;
    ${media.tablet`
      display: inherit !important;
    `};
  }

  .hidden-tablet {
    ${media.tablet`
      display: none !important;
    `};
  }

  .hidden-desktop {
    ${media.desktop`
      display: none !important;
    `};
  }

  .hidden-giant {
    ${media.giant`
      display: none !important;
    `};
  }


  /** --------------------
  *    Helper classes
  * --------------------- */

  .no-margin {
    margin: 0 !important;
  }

  .middot {
    display: inline-flex;
    margin: 0 1rem;
    width: 2px;
    height: 2px;
    border-radius: 50%;
    background-color: var(--gray-3);
  }

  .divider {
    background-color: var(--gray-1);
    border: none;
    display: block;
    height: 1px;
    margin: 0.75rem 0;

    &.large {
      margin: 1.5rem 0;
    }
  }

  .sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    border: 0;
  }

  .rc-tooltip.rc-tooltip-placement-right {
    box-shadow: -5px 0px 30px rgba(0,0,0,0.1);
    background-color: rgba(255,255,255,1);
    border-radius: 8px;
    opacity: 1;

    .rc-tooltip-arrow {
      border-right-color: white;
    }

    .rc-tooltip-inner {
      border-color: transparent;
      background-color: rgba(255,255,255,0.9);

      span {
        font-size: 0.9rem;
        text-transform: uppercase;
        font-weight: 600;
      }
    }
  }
  .rc-tooltip.tooltip-transition-enter,
  .rc-tooltip.tooltip-transition-leave {
    display: block;
  }
  .rc-tooltip.tooltip-transition-enter,
  .rc-tooltip.tooltip-transition-appear {
    opacity: 0;
    animation-duration: 0.3s;
    animation-fill-mode: both;
    animation-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
    animation-play-state: paused;
  }
  .rc-tooltip.tooltip-transition-leave {
    opacity: 1;
    animation-duration: 0.3s;
    animation-fill-mode: both;
    animation-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
    animation-play-state: paused;
  }
  .tooltip-transition-enter.tooltip-transition-enter-active,
  .tooltip-transition-appear.tooltip-transition-appear-active {
    animation-name: tooltipSlideIn;
    animation-play-state: running;
  }
  .tooltip-transition-leave.tooltip-transition-leave-active {
    animation-name: tooltipSlideOut;
    animation-play-state: running;
  }
  @keyframes tooltipSlideIn {
    0% {
      opacity: 0;
      transform: translateX(20px);
    }
    100% {
      opacity: 1;
      transform: translateX(0);
    }
  }
  @keyframes tooltipSlideOut {
    0% {
      opacity: 1;
      transform: translateX(0);
    }
    100% {
      opacity: 0;
      transform: translateX(20px);
    }
  }

  /* Popover Styles */

  .popover {
    &.popover-about {
      background: none;
      padding: 0;

      transform: translateZ(0);
      transition: transform .2s;
      will-change: transform;
      backface-visibility: hidden;

      width: 90%;
      max-width: 440px;
      height: 400px;
      pointer-events: none;

      display: flex;

      .rc-tooltip-arrow {
        display: none;
      }

      .rc-tooltip-content {
        overflow: hidden;
        width: 72px;
        height: 72px;
        border-radius: 0;
        transition: height .4s .1s,width .4s .1s,box-shadow .3s;
        transition-timing-function: cubic-bezier(.67,.17,.32,.95);
        background-color: white;
        border: none;
        box-shadow: var(--box-shadow-container);
        pointer-events: auto;
      }

      &:not(.rc-tooltip-hidden) {
        .rc-tooltip-content {
          width: 100%;
          height: 100%;
          transition-delay: 0s;
        }
      }
    }
  }

  .rc-tooltip.tooltip-transition-expand-enter,
  .rc-tooltip.tooltip-transition-expand-appear {
    .rc-tooltip-inner {
        &:not(.rc-tooltip-hidden) {
          width: 100%;
          height: 100%;
          transition-delay: 0s;
        }
      }
  }
`

export function truncate(width) {
  return `
    max-width: ${width};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `
}

export function roundWidth(columnWidth, gridColumns) {
  const width = Math.floor((100 * columnWidth * 100) / gridColumns / 100)
  return `${width}%`
}
