import styled from 'styled-components/macro'
import { roundWidth } from './style-utils'

const gridColumns = 12

export const FlexGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: calc(var(--grid-gap) * -1) 0 0 calc(var(--grid-gap) * -1);
  justify-content: ${({ justifyContent }) => justifyContent || 'flex-start'};
  align-content: ${({ alignContent }) => alignContent || 'flex-start'};
`

export const Col = styled.div`
  padding: calc(var(--grid-gap)) 0 0 calc(var(--grid-gap));
  background-clip: content-box;
  flex-basis: ${props =>
    props.columns ? roundWidth(props.columns, gridColumns) : '100%'};
  max-width: ${props =>
    props.columns ? roundWidth(props.columns, gridColumns) : '100%'};
`
