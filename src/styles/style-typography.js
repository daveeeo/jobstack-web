import { css } from 'styled-components/macro'
import media from './style-media'

const GlobalStylesTypography = css`
  :root {
    --font-primary: 'Karla', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
    --font-secondary: serif;
    --font-heading: 'Karla', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
      --font-heading-alt: 'Roboto', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;
    --font-logo: 'Roboto', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue',
      sans-serif;

    /* set base values */
    --text-base-size: 1em;
    --text-scale-ratio: 1.2;

    /* type scale */
    --text-xs: calc(1em / (var(--text-scale-ratio) * var(--text-scale-ratio)));
    --text-sm: calc(1em / var(--text-scale-ratio));
    --text-md: calc(1em * var(--text-scale-ratio));
    --text-lg: calc(1em * var(--text-scale-ratio) * var(--text-scale-ratio));
    --text-xl: calc(
      1em * var(--text-scale-ratio) * var(--text-scale-ratio) *
        var(--text-scale-ratio)
    );
    --text-xxl: calc(
      1em * var(--text-scale-ratio) * var(--text-scale-ratio) *
        var(--text-scale-ratio) * var(--text-scale-ratio)
    );
    --text-xxxl: calc(
      1em * var(--text-scale-ratio) * var(--text-scale-ratio) *
        var(--text-scale-ratio) * var(--text-scale-ratio) *
        var(--text-scale-ratio)
    );

    /* line-height */
    --heading-line-height: 1.2;
    --body-line-height: 1.4;

    ${media.tablet`
      --space-unit: 1.25em;
      --component-padding: var(--space-md);
    `};

    /* ${media.desktop`
      --text-base-size: 1.1em;
      --text-scale-ratio: 1.1;
    `}; */
  }

  body {
    font-size: var(--text-base-size);
    font-family: var(--font-primary);
    color: var(--color-text);
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  form legend {
    font-family: var(--font-heading);
    color: var(--color-text-heading);
    margin-top: 0;
    margin-bottom: var(--space-xxs);
    line-height: var(--heading-line-height);
  }

  input,
  textarea,
  select,
  button {
    font-size: var(--text-base-size);
    font-family: var(--font-primary);
    color: var(--color-text);
  }

  /* text size */
  .text--xxxl {
    font-size: var(--text-xxxl);
  }

  h1,
  .text--xxl {
    font-size: var(--text-xxl);
    font-weight: 900;
  }

  h2,
  .text--xl {
    font-size: var(--text-xl);
    font-weight: 900;
  }

  h3,
  .text--lg {
    font-size: var(--text-lg);
    font-weight: 900;
  }

  h4,
  .text--md {
    font-size: var(--text-md);
  }

  .text--sm,
  small {
    font-size: var(--text-sm);
  }

  .text--xs {
    font-size: var(--text-xs);
  }

  p {
    line-height: var(--body-line-height);
  }

  a {
    color: var(--color-link);

    &:visited {
      /* color: var(--color-link-visited); */
    }
  }

  b,
  strong {
    font-weight: bold;
  }

  .text-container {
    h2,
    h3,
    h4 {
      margin-top: var(--space-sm);
    }

    ul,
    ol,
    p {
      margin-bottom: var(--space-md);
    }

    ul,
    ol {
      list-style-position: outside;
      padding-left: 24px;
    }

    ul {
      list-style-type: disc;
    }

    ol {
      list-style-type: decimal;
    }

    ul li,
    ol li {
      line-height: var(--body-line-height);
    }

    em {
      font-style: italic;
    }

    u {
      text-decoration: underline;
    }
  }

  .title {
    font-family: var(--font-heading-alt);
    margin-bottom: 1.5rem;
  }

  .subtitle {
    font-size: 1.05rem;
    line-height: 1.7;
  }

  /* utility classes */
  .truncate {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  .text-uppercase {
    text-transform: uppercase;
  }

  .text-center {
    text-align: center;
  }
`
export default GlobalStylesTypography
